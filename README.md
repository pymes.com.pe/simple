# README #

Generate websites

## APPS ##
* Python 3.5 >
* pip install -r requirements.txt

## SYSTEM REQUIREMENT ##
* apt-get install zlib1g-dev
* apt-get install libjpeg-dev
* `apt-get install apt-get install libmariadbclient-dev`


### What is this repository for? ###

* 1.0 : Estable
* 1.2 : More feature

### Clone ####
Clone for production
`git clone git@gitlab.com:pymes.com.pe/simple.git -b prod`

### How do I get set up? ###

* Create data base
* Clone simple
* Install the apps
* Create configuration file simple/config.py
* RUN:
    $ `python manage.py makemigrations account common business cms webform`
* python manage.py migrate
* python manage.py createsuperuser
* Crear virtualhost

## initial install ##
* Install pip3
> `apt-get install python3-pip`
> `pip3 install virtualenv`

* Crear virtualenv
> `virtualenv -p python3 venv`
## APACHE 2.4 ##
Instalar apache2.
`$ apt-get install apache2`
Instalar apache2 - python (MOD_WSGI)
`$ apt-get install libapache2-mod-wsgi`
