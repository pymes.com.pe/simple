# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from business.models import Web


class account(models.Model):
    email = models.EmailField(max_length=128)
    password = models.CharField(max_length=32, editable=False)
    web_id = models.IntegerField(editable=False)
    create_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.email


