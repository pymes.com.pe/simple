from django.conf.urls import url
from mailer.views import servers

urlpatterns = [
    # serversa
    url(r'^servers/$', servers.index, name='servers'),
    url(r'^servers/(?P<pk>\d+)/edit$', servers.edit, name='servers_edit'),
    url(r'^servers/new$', servers.new, name='servers_new'),
]