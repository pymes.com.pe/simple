from django.contrib.auth.models import User
from django.test import TestCase
import unittest
from django.test import Client
from django.urls import reverse
from account import models


class AccountTestCase(TestCase):
    def setUp(self):
        # Crear usuario
        user = User.objects.create(username='juan')
        user.set_password('juan132')
        user.is_superuser = True
        user.save()

    def test_guest(self):
        # Form 200
        response = self.client.get(reverse("login"))
        self.assertEqual(response.status_code, 200)
        # Form 200
        response = self.client.get(reverse("signup"))
        self.assertEqual(response.status_code, 200)
        # Logout
        response = self.client.get(reverse("logout"))
        self.assertEqual(response.status_code, 302)
        # Form 302
        response = self.client.get(reverse("profile_change_password"))
        self.assertEqual(response.status_code, 302)

    def test_profile(self):
        # Autenticar usuario
        self.client = Client()
        self.client.login(username='juan', password='juan132')
        # Form 200
        response = self.client.get(reverse("profile"))
        self.assertEqual(response.status_code, 200)
        # Form 200
        response = self.client.get(reverse("profile_contact"))
        self.assertEqual(response.status_code, 200)
        # Form 200
        response = self.client.get(reverse("profile_change_password"))
        self.assertEqual(response.status_code, 200)