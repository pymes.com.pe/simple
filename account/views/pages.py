# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth.models import User
from django.urls import reverse
from django.http.response import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from account.forms import SignUpForm, NewAccountForm
from django.contrib import messages

def signup(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect("/accounts")
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            username = username.lower()
            password = form.cleaned_data["password"]
            email = form.cleaned_data["email"]
            user = User.objects.create_user(username, email, password)

            messages.info(request, 'Gracias. Puede Iniciar sesion')
            return HttpResponseRedirect("/accounts/login/")
    else:
        form = SignUpForm()
    ctx = {
        "form":form,
        "base":"layout/base.html",
    }
    return render(request,'accounts/signup.html', ctx)

def active_account(request, token_active, user_pk):
    if request.user.is_authenticated:
        return HttpResponseRedirect("/accounts")
    try:
        user = User.objects.get(user_id=user_pk)
    except User.DoesNotExist:
        HttpResponse("Account not found")

    if user.token != token_active:
        messages.info(request,"Codigo no disponible")
        HttpResponseRedirect(reverse("login"))

    if user.user.is_active:
        messages.info(request,"Genial. Su cuenta ya esta activa")
        HttpResponseRedirect(reverse("login"))

    # Form for create user and password
    if request.method == "POST":
        form = NewAccountForm(request.POST)
        if form.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password")
            user.user.username = username
            user.user.set_password(password)
            user.user.is_active = True
            user.user.save()

            messages.info(request,"Genial. Su cuenta se a creado satisfactoriamente.")
            return HttpResponseRedirect(reverse("login"))
    else:
        form = NewAccountForm()
    ctx = {
        "form":form,
        "base":"layout/base.html"
    }
    return render(request,"accounts/new_account.html",ctx)