# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth.models import User
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from account.forms import ContactProfileForm, ChangePasswordForm
from django.contrib import messages
from business.models import Web


def get_ip(request):
    ip = request.META.get("HTTP_X_FORWARDED_FOR", None)
    if ip:
        ip = ip.split(", ")[0]
    else:
        ip = request.META.get("REMOTE_ADDR", "")
    return ip

@login_required()
def index(request):
    obj = request.user
    webs = Web.objects.filter(owner_id=obj.id)[:2]
    ctx = {
        "obj":obj,
        "webs":webs,
        "base":"layout/user.html",
    }
    return render(request,"profile/detail.html", ctx)


@login_required()
def edit_contact(request):
    obj = request.user

    if request.method == 'POST':
        form = ContactProfileForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Information updated.")
            return HttpResponseRedirect(reverse("profile"))
        else:
            messages.warning(request, "Complete the required fields.")
    else:
        form = ContactProfileForm(instance=obj)
    ctx = {
        "form":form,
        "base":"layout/user.html",
        }
    return render(request,"profile/edit.html", ctx)

@login_required()
def change_password(request):
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST)
        if form.clean:
            user = request.user
            password = request.POST.get("password")
            new_password = request.POST.get("new_password")
            new_password_verify = request.POST.get("new_password_verify")

            success = user.check_password(password)
            if success :
                if new_password and new_password != new_password_verify:
                    messages.info(request, "New password do not macth")
                    return HttpResponseRedirect(reverse("profile_change_password"))
                user.set_password(new_password)
                user.save()
                messages.info(request, "Password changed. Login with your new password")
                return HttpResponseRedirect(reverse("profile"))
            else:
                messages.warning(request, "Incorrect password")
    else:
        form = ChangePasswordForm()
    ctx =  {
        "form": form,
        "base":"layout/user.html",
    }
    return render(request,"profile/password.html",ctx)