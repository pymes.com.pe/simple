from django.conf.urls import url
from account.views import pages, profiles
from django.contrib.auth import views as auth_views

urlpatterns = [
    url('^login/$', auth_views.login, name='login'),
    url('^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url('^signup$', pages.signup, name="signup"),
    url(r'^new_active/(?P<token_active>\w{32})/(?P<user_pk>\d+).php', pages.active_account, name='www_active_account'),
    # Profile
    url('^profile/$', profiles.index, name="profile"),
    url('^update$', profiles.edit_contact, name="profile_contact"),
    url('^change_password$', profiles.change_password, name="profile_change_password"),
    # Password
    url('^password_reset/$', auth_views.password_reset, name='password_reset'),
    url('^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
]