# -*- coding: utf-8 -*-
from django.forms import ValidationError, CharField, PasswordInput, Form, ModelForm
from django.contrib.auth.models import User


class UserForm(ModelForm):
    username = CharField(required=True,min_length=3,max_length=15)
    password = CharField(required=True,min_length=8,max_length=16,widget=PasswordInput)
    email = CharField(required=True,max_length=60,label="E-mail")
    class Meta:
        model = User
        fields = ('username','email','first_name','last_name', 'is_superuser','is_active')



class UserEditForm(ModelForm):
    class Meta:
        model = User
        fields = ('username','email','first_name','last_name', 'is_superuser','is_active')



class SignUpForm(ModelForm):
    username = CharField(required=True,min_length=3,max_length=15)
    password = CharField(required=True,min_length=8,max_length=16,widget=PasswordInput)
    email = CharField(required=True,max_length=60,label="E-mail")
    class Meta:
        model = User
        fields = ['username', 'password', 'email']


class NewAccountForm(ModelForm):
    username = CharField(required=True,min_length=3,max_length=32)
    password = CharField(required=True,min_length=8,max_length=16,widget=PasswordInput)
    password_confirmed = CharField(required=True,min_length=8,max_length=16,widget=PasswordInput)

    class Meta:
        model = User
        fields = ['username','password','password_confirmed']

    def clean(self):
        password = self.cleaned_data.get('password')
        password_confirmed = self.cleaned_data.get('password_confirmed')

        if password and password != password_confirmed:
            raise ValidationError("Passwords don't match")

        return self.cleaned_data


class ContactProfileForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name']

    def clean_email(self):
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')

        if email and User.objects.filter(email=email).exclude(username=username).exists():
            raise ValidationError(u'Email addresses must be unique.')

        return email


class ChangePasswordForm(Form):
    password = CharField(required=True,min_length=8,max_length=16,widget=PasswordInput,label="Last password")
    new_password = CharField(required=True,min_length=8,max_length=16,widget=PasswordInput,label="New password")
    new_password_verify = CharField(required=True,min_length=8,max_length=16,widget=PasswordInput,label="Repeat new password")

    def clean(self):
        new_password = self.cleaned_data.get("new_password")
        new_password_verify = self.cleaned_data.get("new_password_verify")
        if new_password and new_password != new_password_verify:
            raise ValidationError("New password do not match")
        return self.cleaned_data


class UserPasswordForm(Form):
    password = CharField(required=True,min_length=8,max_length=16)