"""simple URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from business.views import webs
from www.views import nodes, pages, fronts, domains, webforms

urlpatterns = [
    url(r'^$', webs.user, name='dashboard'),
    # API
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/info$', domains.info, name='web_info'),
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/languages$', domains.languages, name='web_languages'),
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/sitemap$', domains.sitemap_all, name='web_sitemap_all'),
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/sitemap/(?P<lang_code>es|en|pt-br)/$', domains.sitemap, name='web_sitemap'),
    # Page for languages
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/-page$', pages.web_page_default, name='web_page_default'),
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/(?P<lang_code>es|en|pt-br)-page$', pages.web_page_laguage, name='web_page_language'),

    # Webforms
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/webform/contact_us/$', webforms.contact_us, name='web_contact_us'),
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/webform/book_tour/$', webforms.book_tour, name='web_book_tour'),
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/webform/book_room/$', webforms.book_room, name='web_book_room'),
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/webform/suscription/$', webforms.suscription, name='web_suscription'),
    # Fronts (es|en|pt-br)
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/p/(?P<lang_code>es|en|pt-br)-json$', fronts.web_home_redirect, name='web_home_redirect'),
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/p/-json$', fronts.web_default_language, name='web_default_home'),
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/p/(?P<lang_code>es|en|pt-br)/-json$', fronts.web_home, name='web_home'),
    # Nodes
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/p/(?P<lang_code>es|en|pt-br)/(?P<slug>[-\w\.]+)-json$', nodes.web_node, name='web_node'),
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/p/(?P<lang_code>es|en|pt-br)/(?P<slug>[-\w\.]+)/-json$', nodes.web_node, name='web_node_slash'),
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/p/not_found/(?P<lang_code>es|en|pt-br)/(?P<slug>[-\w\.]+)-json$', nodes.not_found, name='web_not_found'),
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/p/not_found/(?P<lang_code>es|en|pt-br)/-json$', nodes.home_not_found, name='web_home_not_found'),
    url(r'^api/(?P<web_code>\w{32})/(?P<web_pk>\d+)/p/not_found/(?P<slug>[-\w\.]+)-json$', nodes.not_found, name='web_not_found'),
    # CMS and Account
    url(r'^web/(?P<web_pk>\d+)/', include('cms.urls')),
    url(r'^accounts/', include("account.urls")),
    # Super User
    url(r'^business/', include("business.urls")),
]
