Consulta de reserva desde {{web.name}}

TIPO DE HABITACION: {{message.room}}
CHECK IN: {{message.check_in}}
CHECK OUT: {{message.check_out}}
ADULTOS : {{message.adults}}

DE : {{message.name}}
E-MAIL {{message.email}}
{% if message.phone %}TELEFONO : {{message.phone}}{% endif %}
MENSAJE:
{{message.message}}


-- atte --
{{web.name}}