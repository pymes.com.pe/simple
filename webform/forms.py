# -*- coding: utf-8 -*-
from django.forms import ModelForm
from webform.models import Contact


class ContactForm(ModelForm):
    class Meta:
        model = Contact
        exclude = ('estado',)
