# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Contact(models.Model):
    name = models.CharField(max_length=96, verbose_name=u"Nombre completo")
    email = models.EmailField(max_length=96, verbose_name=u"E-mail")
    phone = models.CharField(max_length=32, blank=True, verbose_name=u"Teléfono")
    subject = models.CharField(max_length=64, verbose_name=u"Asunto")
    message = models.TextField(verbose_name=u"Mensanje")
    # others
    read = models.BooleanField(default=False, editable=False)
    create_at = models.DateTimeField(auto_now_add=True)
    web_id = models.IntegerField(editable=False)
    ip = models.GenericIPAddressField(null=True, blank=True)

    class Meta:
        ordering = ["-id"]

    def __str__(self):
        return self.subject

class BookRoom(models.Model):
    check_in = models.DateField()
    check_out = models.DateField()
    room = models.CharField(max_length=64)
    adults = models.CharField(max_length=2)
    children = models.CharField(max_length=2, blank=True, default=0)
    # Guest
    name = models.CharField(max_length=124)
    email = models.EmailField(max_length=124)
    phone = models.CharField(max_length=32, blank=True)
    country = models.CharField(max_length=2, blank=True)
    message = models.TextField(blank=True)
    # others
    read = models.BooleanField(default=False, editable=False)
    create_at = models.DateTimeField(auto_now_add=True)
    web_id = models.IntegerField(editable=False)

    def __str__(self):
        return "Book Hotel"

    class Meta:
        ordering = ["-id"]

class BookTour(models.Model):
    # Guest
    name = models.CharField(max_length=124)
    email = models.EmailField(max_length=124)
    phone = models.CharField(max_length=32, blank=True)
    country = models.CharField(max_length=64, blank=True)
    # tour information
    tour = models.CharField(max_length=128)
    tour_id = models.IntegerField(default=0, blank=True)
    journey_date = models.DateField()
    adults = models.CharField(max_length=2)
    children = models.CharField(max_length=2, default=0, blank=True)
    duration = models.CharField(max_length=2, default=0, blank=True)
    destinations = models.CharField(max_length=64, blank=True)
    experiences = models.CharField(max_length=64, blank=True)
    hotel_category = models.CharField(max_length=64, blank=True)
    message = models.TextField(blank=True)
    # others
    read = models.BooleanField(default=False, editable=False)
    create_at = models.DateTimeField(auto_now_add=True)
    web_id = models.IntegerField(editable=False)

    def __str__(self):
        return "Book Tour"

    class Meta:
        ordering = ["-id"]

class Suscription(models.Model):
    email = models.EmailField(max_length=64)
    web_id = models.IntegerField(editable=False)

    def __str__(self):
        return self.email

    class Meta:
        ordering = ["-id"]