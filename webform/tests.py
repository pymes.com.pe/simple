from django.contrib.auth.models import User
from django.test import TestCase
import unittest
from django.test import Client
from django.urls import reverse
from webform import models
from business.models import Web, Template, Color


class WebformTestCase(TestCase):
    def setUp(self):
        # Crear usuario
        user = User.objects.create(username='juan')
        user.set_password('juan132')
        user.is_superuser = True
        user.save()
        # Autenticar usuario
        self.client = Client()
        self.client.login(username='juan', password='juan132')
        # Template
        Template.objects.create(name="First Template")
        Color.objects.create(hexadecimal="aabbcc", template_id=1)
        Web.objects.create(domain="pymes.com.pe", owner_id=1)
        # Web
        domain_pk = 1
        models.Contact.objects.create(web_id=domain_pk,name="Juan", email="juan@maspid.com", subject="Test 1", message="This is a test message")


    def test_contacts(self):
        domain_pk = 1
        # List 200
        response = self.client.get(reverse("webforms", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # Delete 200
        response = self.client.get(reverse("webforms_delete", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Delete 302
        response = self.client.get(reverse("webforms_delete", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # Detail 200
        response = self.client.get(reverse("webforms_detail", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Detail 302
        response = self.client.get(reverse("webforms_detail", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)