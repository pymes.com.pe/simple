from django.contrib.auth.models import User
from django.test import TestCase
import unittest
from django.test import Client
from django.urls import reverse
from cms import models
from business.models import Web, Template, Color
from common.models import Redirect, NotFound
from webform.models import Contact, BookRoom, BookTour

class CmsTestCase(TestCase):
    def setUp(self):
        # Crear usuario
        user = User.objects.create(username='juan')
        user.set_password('juan132')
        user.is_superuser = True
        user.save()
        # Autenticar usuario
        self.client = Client()
        self.client.login(username='juan', password='juan132')
        # Template
        Template.objects.create(name="First Template")
        Color.objects.create(hexadecimal="aabbcc", template_id=1)
        Web.objects.create(domain="pymes.com.pe", owner_id=1)
        Web.objects.create(domain="paginasweb.cloud", owner_id=1, multi_language=True)
        # Web
        domain_pk = 1
        models.Node.objects.create(web_id=domain_pk,title="Basic Page", in_menu=True)
        models.Network.objects.create(web_id=domain_pk,link="https://facebook.com")
        models.Block.objects.create(web_id=domain_pk,title="Banner 1", body="body")
        models.Slider.objects.create(web_id=domain_pk,title="Slider 1", image="slider.jpg")
        models.Picture.objects.create(web_id=domain_pk,alt="Image", image="juan.jpg")
        models.Document.objects.create(web_id=domain_pk, path="name.pdf", name="juan")
        Redirect.objects.create(web_id=domain_pk, old_path="/old-path", new_path="/new_path")
        #webforms
        Contact.objects.create(web_id=domain_pk, email="juan@pymes.com.pe",name="Juan", subject="subject", message="Message")
        BookRoom.objects.create(web_id=domain_pk, email="juan@pymes.com.pe",name="Juan", room="subject", message="Message", check_in="2017-12-12", check_out="2017-11-11", adults=2)
        BookTour.objects.create(web_id=domain_pk, email="juan@pymes.com.pe",name="Juan", tour="subject", message="Message", journey_date="2017-1-1", adults=2)

    def test_pages(self):
        # New 200
        response = self.client.get(reverse("dashboard"))
        self.assertEqual(response.status_code, 200)
    def test_translations(self):
        domain_pk = 1
        domain_false = 5
        # Welcome 200
        response = self.client.get(reverse("translations_update", args=(domain_pk,1,)))
        self.assertEqual(response.status_code, 200)
        # Welcome 302
        response = self.client.get(reverse("translations_update", args=(domain_false,1,)))
        self.assertEqual(response.status_code, 302)
        # Seo 200
        response = self.client.get(reverse("translations_seo", args=(domain_pk,1,)))
        self.assertEqual(response.status_code, 200)
        # Seo 302
        response = self.client.get(reverse("translations_seo", args=(domain_false,1)))
        self.assertEqual(response.status_code, 302)
        # Seo 200
        response = self.client.get(reverse("translations_new", args=(domain_pk,)))
        self.assertEqual(response.status_code, 302)
        # Seo 200
        response = self.client.get(reverse("translations_new", args=(2,)))
        self.assertEqual(response.status_code, 200)

    def test_appearance(self):
        domain_pk = 1
        domain_false = 5
        # Dashboard 200
        response = self.client.get(reverse("webs_dashboard", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # Dashboard 302
        response = self.client.get(reverse("webs_dashboard", args=(domain_false,)))
        self.assertEqual(response.status_code, 302)
        # Appearence 200
        response = self.client.get(reverse("webs_css_js", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # Appearence 200
        response = self.client.get(reverse("webs_css_js", args=(domain_false,)))
        self.assertEqual(response.status_code, 302)
        # Clear Cache
        response = self.client.get(reverse("clear_cache", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # Appearence 200
        response = self.client.get(reverse("webs_widget", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # Appearence 200
        response = self.client.get(reverse("webs_widget", args=(domain_false,)))
        self.assertEqual(response.status_code, 302)


    def test_nodes(self):
        domain_pk = 1
        # List 200
        response = self.client.get(reverse("nodes", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # Detail 200
        response = self.client.get(reverse("nodes_detail", args=(domain_pk,1,)))
        self.assertEqual(response.status_code, 200)
        # Detail 302
        response = self.client.get(reverse("nodes_detail", args=(domain_pk,40,)))
        self.assertEqual(response.status_code, 302)
        # Detail 302
        response = self.client.get(reverse("nodes_detail", args=(domain_pk,2,)))
        self.assertEqual(response.status_code, 302)
        # Edit 200
        response = self.client.get(reverse("nodes_edit", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Edit 302
        response = self.client.get(reverse("nodes_edit", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # clonar 200
        response = self.client.get(reverse("nodes_clonar", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # clonar 302
        response = self.client.get(reverse("nodes_clonar", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # Edit 200
        response = self.client.get(reverse("nodes_seo", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Edit 302
        response = self.client.get(reverse("nodes_seo", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # New 200
        response = self.client.get(reverse("nodes_new", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # Delete 200
        response = self.client.get(reverse("nodes_delete", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Delete 302
        response = self.client.get(reverse("nodes_delete", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)

    def test_networks(self):
        domain_pk = 1
        # List 200
        response = self.client.get(reverse("networks", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # Edit 200
        response = self.client.get(reverse("networks_edit", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Edit 302
        response = self.client.get(reverse("networks_edit", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # Delete 200
        response = self.client.get(reverse("networks_delete", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Delete 302
        response = self.client.get(reverse("networks_delete", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)

    def test_sliders(self):
        domain_pk = 1
        # List 200
        response = self.client.get(reverse("sliders", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # detail 200
        response = self.client.get(reverse("sliders_detail", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # detail 302
        response = self.client.get(reverse("sliders_detail", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # Edit 200
        response = self.client.get(reverse("sliders_edit", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Edit 302
        response = self.client.get(reverse("sliders_edit", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # Delete 200
        response = self.client.get(reverse("sliders_delete", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Delete 302
        response = self.client.get(reverse("sliders_delete", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # New 200
        response = self.client.get(reverse("sliders_new", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)

    def test_blocks(self):
        domain_pk = 1
        # List 200
        response = self.client.get(reverse("blocks", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # detail 200
        response = self.client.get(reverse("blocks_detail", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # detail 302
        response = self.client.get(reverse("blocks_detail", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # Edit 200
        response = self.client.get(reverse("blocks_edit", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Edit 302
        response = self.client.get(reverse("blocks_edit", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # Delete 200
        response = self.client.get(reverse("blocks_delete", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Delete 302
        response = self.client.get(reverse("blocks_delete", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # New 200
        response = self.client.get(reverse("blocks_new", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)

    def test_documents(self):
        domain_pk = 1
        # List 200
        response = self.client.get(reverse("documents", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # Edit 200
        response = self.client.get(reverse("documents_edit", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Edit 302
        response = self.client.get(reverse("documents_edit", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # Delete 200
        response = self.client.get(reverse("documents_delete", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Delete 302
        response = self.client.get(reverse("documents_delete", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # New 200
        response = self.client.get(reverse("documents_new", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # New 200
        response = self.client.get(reverse("documents_new", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)

    def test_pictures(self):
        domain_pk = 1
        # List 200
        response = self.client.get(reverse("pictures", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # detail 200
        response = self.client.get(reverse("pictures_detail", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # detail 302
        response = self.client.get(reverse("pictures_detail", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # Edit 200
        response = self.client.get(reverse("pictures_edit", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Edit 302
        response = self.client.get(reverse("pictures_edit", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # Delete 200
        response = self.client.get(reverse("pictures_delete", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Delete 302
        response = self.client.get(reverse("pictures_delete", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # New 200
        response = self.client.get(reverse("pictures_new", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # New 200
        response = self.client.get(reverse("pictures_new", args=(domain_pk,1,)))
        self.assertEqual(response.status_code, 200)

    def test_menus(self):
        domain_pk = 1
        # List 200
        response = self.client.get(reverse("menus", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # Edit 200
        response = self.client.get(reverse("menus_edit", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Edit 302
        response = self.client.get(reverse("menus_edit", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # Delete 200
        response = self.client.get(reverse("menus_delete", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Delete 302
        response = self.client.get(reverse("menus_delete", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)

    def test_redirects(self):
        domain_pk = 1
        # List 200
        response = self.client.get(reverse("redirects", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # Edit 200
        response = self.client.get(reverse("redirects_edit", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Edit 302
        response = self.client.get(reverse("redirects_edit", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)
        # Delete 200
        response = self.client.get(reverse("redirects_delete", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # Delete 302
        response = self.client.get(reverse("redirects_delete", args=(domain_pk,2)))
        self.assertEqual(response.status_code, 302)

    def test_reports(self):
        domain_pk = 1
        # List 200
        response = self.client.get(reverse("reports_notfound", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)

    def test_webforms(self):
        domain_pk = 1
        # List 200
        response = self.client.get(reverse("webforms", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # List 200
        response = self.client.get(reverse("webforms_detail", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # List 200
        response = self.client.get(reverse("webforms_delete", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # List 200
        response = self.client.get(reverse("webforms_rooms", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # List 200
        response = self.client.get(reverse("webforms_rooms_detail", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)
        # List 200
        response = self.client.get(reverse("webforms_tours", args=(domain_pk,)))
        self.assertEqual(response.status_code, 200)
        # List 200
        response = self.client.get(reverse("webforms_tours_detail", args=(domain_pk,1)))
        self.assertEqual(response.status_code, 200)