# -*- coding: utf-8 -*-
from django.db import models
from django.template.defaultfilters  import slugify
from django.db.models.signals import post_save
from django.dispatch import receiver
from common import models as choices_list
from business.models import Web
from business.models import Color, Template
from re import sub
from stdimage.models import StdImageField

@receiver(post_save, sender=Web)
def create_web_appearance(sender, instance, created, **kwargs):
    if created:
        Appearance.objects.create(web_id=instance.pk)

@receiver(post_save, sender=Web)
def create_web_translation(sender, instance, created, **kwargs):
    if created:
        Translation.objects.create(web_id=instance.id, language=instance.language)

def web_media_dir(instance, filename):
    new_filename = sub('[^-a-zA-Z0-9_.() ]+', '', filename)
    return u'ws_{0}/media/{1}'.format(instance.web_id, filename)

def web_docs_dir(instance, filename):
    new_filename = sub('[^-a-zA-Z0-9_.() ]+', '', filename)
    return u'ws_{0}/docs/{1}'.format(instance.web_id, new_filename)

class Appearance(models.Model):
    web_id = models.IntegerField(primary_key=True, editable=False)
    # Appearence
    copyright = models.CharField(max_length=128, blank=True)
    background = models.ImageField(
        upload_to=web_media_dir,
        blank=True,
        default="background.png",
        verbose_name=u"Fondo"
        )
    video = models.URLField(max_length=128, blank=True, verbose_name=u"URL de Video")
    featured = models.TextField(max_length=1024, blank=True)
    css = models.TextField(max_length=720, blank=True)
    js = models.TextField(max_length=1024, blank=True)
    widgets = models.TextField(max_length=720, blank=True)
    social_widgets = models.TextField(max_length=720, blank=True)
    google_map = models.TextField(max_length=384, blank=True)
    power_by = models.CharField(max_length=120, default=u"creado con Pymes.com.pe")
    # General
    google_analytics = models.CharField(max_length=32, blank=True)
    facebook_pixel = models.CharField(max_length=32, blank=True, verbose_name=u"Facebook pixel ID")
    captcha = models.BooleanField(default=False)
    is_slider_full = models.BooleanField(default=False, verbose_name=u"Slider pantalla completa")
    def __str__(self):
        return "Appearence"


class Network(models.Model):
    link = models.URLField(max_length=128, verbose_name=u"Red social")
    weight = models.SmallIntegerField(default=0, verbose_name=u"Orden")
    web_id = models.IntegerField(editable=False)
    create_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.link

    class Meta:
        ordering = ["weight"]


class Picture(models.Model):
    image = StdImageField(
        upload_to=web_media_dir,
        blank=True,
        variations={
            'medium': (640, 400, True),
            'thumb': (360, 240, True),
        },
        default="node.png",
        verbose_name=u"Imagen")
    alt = models.CharField(max_length=200, verbose_name=u"Descripción")
    weight = models.SmallIntegerField(default=0, verbose_name=u"Orden")
    web_id = models.IntegerField(editable=False)
    create_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["weight"]


class Translation(models.Model):
    web_id = models.IntegerField(editable=False)
    language = models.CharField(
        max_length=5,
        default="es",
        choices=choices_list.LANGUAGES,
        verbose_name=u"Idioma"
        )
    # Content
    title = models.CharField(max_length=128, verbose_name=u"Título")
    body = models.TextField(blank=True, verbose_name=u"Contenido")
    image = models.ImageField(
        upload_to=web_media_dir,
        blank=True,
        default="home.jpg",
        verbose_name=u"Imagen")
    link_title = models.CharField(
        max_length=64,
        default=u"Leer más",
        verbose_name=u"Título de enlace"
        )
    link_url = models.CharField(
        max_length=128,
        blank=True,
        verbose_name=u"URL de enlace"
        )
    featured = models.TextField(max_length=1024, blank=True)
    # urls
    book_hotel = models.CharField(max_length=128, default="/", verbose_name=u"Reservar hotel")
    book_tour = models.CharField(max_length=128, default="/", verbose_name=u"Reservar tour")
    contact_us = models.CharField(max_length=128, default="/", verbose_name=u"Contáctenos")
    # Blocks
    section_first = models.TextField(blank=True)
    section_second = models.TextField(blank=True)
    footer = models.TextField(blank=True)
    # Seo
    page_title = models.CharField(max_length=128, blank=True)
    description = models.CharField(max_length=250, blank=True)
    keywords = models.CharField(max_length=128, blank=True)
    status =  models.BooleanField(choices=choices_list.DOMAIN_STATUS, default=True, verbose_name=u"Estado")
    # info
    slogan = models.CharField(max_length=200, blank=True)
    business_hours = models.CharField(max_length=48, blank=True, verbose_name=u'Horario de trabajo')
    address = models.CharField(max_length=200, blank=True, verbose_name=u"Dirección")
    city = models.CharField(max_length=40, blank=True, verbose_name=u"Ciudad - País")

    class Meta:
        unique_together = ("web_id", "language")
        index_together = ["web_id", "language"]

    def save(self, *args, **kwargs):

        if not self.page_title:
            self.page_title = self.title

        if not self.description:
            self.description = self.page_title

        if not self.keywords:
            self.keywords = self.page_title

        super(Translation,self).save(*args,**kwargs)


class Slider(models.Model):
    title = models.CharField(max_length=64, blank=True, verbose_name=u"Título")
    language = models.CharField(
        max_length=5,
        choices=choices_list.LANGUAGES,
        default="es",
        verbose_name=u"Idioma")
    body = models.CharField(max_length=128, blank=True, verbose_name=u"Contenido")
    link_title = models.CharField(
        max_length=64,
        default=u"Leer más",
        verbose_name=u"Título de enlace"
        )
    link_url = models.URLField(
        max_length=128,
        blank=True,
        verbose_name=u"URL de enlace"
        )
    image = StdImageField(
        upload_to=web_media_dir,
        blank=True,
        variations={
            'medium': (720, 320, True),
            'thumb': (420, 260, True),
        },
        default = "slider.png",
        verbose_name = u"Imagen"
    )
    status = models.BooleanField(
        default=True,
        choices=choices_list.STATUS,
        verbose_name=u"Estado"
        )
    weight = models.SmallIntegerField(default=0, verbose_name=u"Orden")
    web_id = models.IntegerField(editable=False)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["weight"]


class Node(models.Model):
    title = models.CharField(max_length=128, verbose_name=u"Título")
    money = models.CharField(
        max_length=3,
        default="USD",
        choices=choices_list.MONEYS,
        verbose_name=u"Moneda"
        )
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0, verbose_name=u"Precio")
    quantity = models.CharField(max_length=64, blank=True, verbose_name=u"Duración")
    with_image = models.BooleanField(
        default=True,
        choices=choices_list.WITH_IMAGE,
        verbose_name=u"Con imagen"
        )
    image = StdImageField(
        upload_to=web_media_dir,
        blank=True,
        variations={
            'medium': (640, 400, True),
            'thumb': (360, 240, True),
        },
        default = "node.png",
        verbose_name = u"Imagen"
        )
    language = models.CharField(
        max_length=5,
        choices=choices_list.LANGUAGES,
        default="es",
        verbose_name=u"Idioma")
    summary = models.CharField(max_length=255, blank=True, verbose_name=u"Resumen")
    body = models.TextField(blank=True, verbose_name=u"Contenido", default=" ")
    include = models.TextField(blank=True, verbose_name=u"Incluye / No Incluye", default=" ")
    recommendations = models.TextField(blank=True, verbose_name=u"Recomendaciones", default=" ")
    promoted = models.BooleanField(default=False, verbose_name=u"Promover")
    front = models.BooleanField(default=False, verbose_name=u"En página principal")
    node_type = models.CharField(
        max_length=2,
        choices=choices_list.NODE_TYPE,
        default="p",
        verbose_name=u"Tipo de contenido"
        )
    category = models.ForeignKey(
        "Node",
        null=True,
        on_delete=models.SET_NULL,
        blank=True,
        related_name="node_category",
        verbose_name=u"Categoria"
        )
    tags = models.CharField(max_length=64, blank=True)
    status = models.BooleanField(
        default=True,
        choices=choices_list.STATUS,
        verbose_name=u"Estado"
        )
    # Seo
    page_title = models.CharField(max_length=128, blank=True)
    description = models.CharField(max_length=200, blank=True)
    keywords = models.CharField(max_length=200, blank=True)
    slug = models.CharField(max_length=200, db_index=True)

    web_id = models.IntegerField(editable=False)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)
    # MENU
    weight = models.SmallIntegerField(default=0, verbose_name=u"Orden")
    in_menu = models.BooleanField(default=False, verbose_name="Pertenece a menu")
    images = models.ManyToManyField(Picture, blank=True)
    robot_index = models.BooleanField(default=True, verbose_name="Robot Index")
    amp = models.BooleanField(default=False, verbose_name="AMP")
    script = models.CharField(max_length=128, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        unique_together = ("web_id", "slug",'language')
        index_together = ["web_id", "slug"]
        ordering = ['id']

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = "{0}{1}".format(slugify(self.title),".html")
            if Node.objects.filter(web_id=self.web_id, language=self.language, slug=self.slug).exists():
                return True

        if not self.page_title:
            self.page_title = self.title

        if not self.description:
            self.description = self.title

        if not self.keywords:
            self.keywords = self.title

        self.title = self.title[:128]

        super(Node,self).save(*args,**kwargs)

    def uri(self):
        return "/{0}/{1}".format(self.language,self.slug)

    def get_price(self):
        if self.price < 0.01:
            return 0
        return self.price


@receiver(post_save, sender=Node)
def create_node_menu(sender, instance, created, **kwargs):
    if created and instance.in_menu:
        Menu.objects.create(
                node_id=instance.pk,
                title=instance.title,
                language=instance.language,
                slug=instance.slug,
                web_id=instance.web_id,
                )


class Menu(models.Model):
    node = models.OneToOneField(Node, null=True, blank=True, on_delete=models.SET_NULL)
    title = models.CharField(
        max_length=64,
        verbose_name=u"Título"
        )
    description = models.CharField(max_length=64)
    language = models.CharField(max_length=5, default="es", verbose_name=u"Lenguaje", choices=choices_list.LANGUAGES)
    slug = models.CharField(max_length=200)
    parent = models.ForeignKey(
        "Menu",
        null=True,
        on_delete=models.SET_NULL,
        blank=True,
        verbose_name=u"Menu padre"
        )
    weight = models.SmallIntegerField(default=0, verbose_name=u"Orden")
    expanded = models.BooleanField(default=False, verbose_name=u"Expandido")
    web_id = models.IntegerField(editable=False)

    class Meta:
        ordering = ["weight"]

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.description:
            self.description = self.title
        self.title = self.title[:64]
        super(Menu,self).save(*args,**kwargs)

    def uri(self):
        if self.node:
            return "/{0}/{1}".format(self.language,self.slug)
        else:
            return "{}".format(self.slug)

    def delete(self):
        subitems = Menu.objects.filter(parent_id=self.pk)
        for subitem in subitems:
            subitem.parent_id = None
            subitem.save()
        super(Menu, self).delete()


class Block(models.Model):
    name = models.CharField(max_length=32, blank=True, verbose_name=u"Nombre")
    language = models.CharField(max_length=5, default="es", verbose_name=u"Lenguaje")
    title = models.CharField(max_length=128, blank=True, verbose_name=u"Título")
    body = models.TextField(verbose_name=u"Contenido")
    image = models.ImageField(
        upload_to=web_media_dir,
        blank=True,
        default="block.png",
        verbose_name=u"Imagen"
        )
    front = models.BooleanField(default=True, verbose_name=u"Sólo en inicio")
    block_type = models.CharField(
        max_length=1,
        choices=choices_list.BLOCK_TYPE,
        default="t",
        verbose_name=u"Formato"
        )
    css_class = models.CharField(max_length=64, blank=True, verbose_name=u"CSS Class")
    css_style = models.TextField(blank=True)
    selector_left = models.CharField(max_length=32, blank=True, default="col-md-6", verbose_name="Izquierda")
    selector_right = models.CharField(max_length=32, blank=True, default="col-md-6", verbose_name="Derecha")
    weight = models.SmallIntegerField(default=0, verbose_name=u"Orden")
    web_id = models.SmallIntegerField(editable=False)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ["weight"]

    def save(self, *args, **kwargs):
        if not self.name:
            if self.title:
                self.name = slugify(self.title)
            else:
                self.name = "block"
        self.name = self.name[:30]
        super(Block,self).save(*args,**kwargs)


class Document(models.Model):
    path = models.FileField(upload_to=web_docs_dir, verbose_name=u"Archivo")
    name = models.CharField(max_length=128, blank=True, verbose_name=u"Nombre")
    web_id = models.IntegerField(editable=False)
    create_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["id"]

    def __str__(self):
        return "{0}".format(self.id)