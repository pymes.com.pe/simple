# -*- coding: utf-8 -*-
from django.forms import ModelForm, Textarea, Select, TextInput
from cms.models import Translation, Appearance, Node, Slider, Network, Block, Picture, Document, Menu
from bs4 import BeautifulSoup

class TranslationForm(ModelForm):
    class Meta:
        model = Translation
        fields = ('language','title','body','image','link_title','link_url', 'book_hotel', 'book_tour', 'contact_us','status')

    """
    def clean_body(self):
        data = self.cleaned_data['body']

        clean_data = BeautifulSoup(data, 'html.parser')
        for match in clean_data.findAll('span'):
            match.unwrap()

        return clean_data;
    """

class TranslationEditForm(ModelForm):
    class Meta:
        model = Translation
        fields = ('title','body','language','image','link_title','link_url', 'book_hotel', 'book_tour', 'contact_us','status')

    """
    def clean_body(self):
        data = self.cleaned_data['body']

        clean_data = BeautifulSoup(data, 'html.parser')
        for match in clean_data.findAll('span'):
            match.unwrap()

        return clean_data;
    """

class StructureForm(ModelForm):
    class Meta:
        model = Translation
        fields = ('slogan','business_hours','address','city', 'book_hotel', 'book_tour', 'contact_us','featured','footer','section_second','section_first')

class TranslationSeoForm(ModelForm):
    class Meta:
        model = Translation
        fields = ('page_title','description','keywords')
        widgets = {
            'page_title': TextInput(attrs={'data-length':60}),
            'description': Textarea(attrs={'data-length':160})
        }

class CssJsForm(ModelForm):
    class Meta:
        model = Appearance
        fields = ('css','js','widgets','google_analytics','facebook_pixel')

class WidgetForm(ModelForm):
    class Meta:
        model = Appearance
        fields = ('is_slider_full', 'widgets', 'social_widgets', 'google_map','copyright', 'power_by', 'google_analytics', 'facebook_pixel', 'background')


class NetworkForm(ModelForm):
    class Meta:
        model = Network
        exclude = ('exclude',)


class SliderForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(SliderForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        if instance.web_id:
            languages =list()
            for x in Translation.objects.filter(web_id=instance.web_id).values('id','language'):
                e = [x["language"],x["language"]]
                languages.append(e)

            self.fields["language"].choices = languages

    class Meta:
        model = Slider
        exclude = ('state',)

class MenuForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MenuForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        if instance.web_id:
            if instance.language:
                self.fields["parent"].queryset = Menu.objects.filter(parent__isnull=True, web_id=instance.web_id, language=instance.language)
            else:
                self.fields["parent"].queryset = Menu.objects.filter(parent__isnull=True, web_id=instance.web_id)

    class Meta:
        model = Menu
        fields = ('title','slug','language','parent','weight','expanded')


class BlockForm(ModelForm):
    class Meta:
        model = Block
        exclude = ('exclude',)


class PictureForm(ModelForm):
    class Meta:
        model = Picture
        exclude = ('exclude',)

class DocumentForm(ModelForm):
    class Meta:
        model = Document
        exclude = ('state',)

class DocumentEditForm(ModelForm):
    class Meta:
        model = Document
        fields = ('name',)