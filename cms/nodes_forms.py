# -*- coding: utf-8 -*-
from django.forms import ModelForm, Textarea, Select, CharField, TextInput
from business.models import Template
from cms.models import Node, Translation
from bs4 import BeautifulSoup
import string

class ContentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ContentForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        if instance.web_id:
            self.fields["category"].queryset = Node.objects.filter(node_type="c", web_id=instance.web_id)
            languages =list()
            for x in Translation.objects.filter(web_id=instance.web_id).values('id','language'):
                e = [x["language"],x["language"]]
                languages.append(e)

            self.fields["language"].choices = languages

    def clean_body(self):
        valid_characters = string.printable
        data = self.cleaned_data['body']
        start_string = data
        end_string = ''.join(i for i in start_string if i in valid_characters)

        return end_string;

    class Meta:
        model = Node
        fields = ('title','language','node_type','summary','body','include','category','in_menu','promoted','front')
        widgets = {
            'summary': Textarea(),
            'language': Select(),
        }


class ImageForm(ModelForm):
    class Meta:
        model = Node
        fields = ('with_image','image',)

class ContentAllForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ContentAllForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        if instance.web_id:
            self.fields["category"].queryset = Node.objects.filter(node_type="c", web_id=instance.web_id)
            languages =list()
            for x in Translation.objects.filter(web_id=instance.web_id).values('id','language'):
                e = [x["language"],x["language"]]
                languages.append(e)

            self.fields["language"].choices = languages

    def clean_body(self):
        valid_characters = string.printable
        data = self.cleaned_data['body']
        start_string = data
        end_string = ''.join(i for i in start_string if i in valid_characters)

        return end_string;

    class Meta:
        model = Node
        exclude = ('slug',)
        widgets = {
            'summary': Textarea(),
            'language': Select(),
        }


class PageForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(PageForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        if instance.web_id:
            self.fields["category"].queryset = Node.objects.filter(node_type="c", web_id=instance.web_id)
            languages =list()
            for x in Translation.objects.filter(web_id=instance.web_id).values('id','language'):
                e = [x["language"],x["language"]]
                languages.append(e)

            self.fields["language"].choices = languages

    def clean_body(self):
        valid_characters = string.printable
        data = self.cleaned_data['body']
        start_string = data
        end_string = ''.join(i for i in start_string if i in valid_characters)

        return end_string;

    class Meta:
        model = Node
        fields = ('title','language','summary','body','category','in_menu','promoted','front')
        widgets = {
            'summary': Textarea(),
            'language': Select(),
        }

class ContentSeoForm(ModelForm):
    class Meta:
        model = Node
        fields = ('page_title','description','keywords','front','amp','robot_index','slug','promoted','front','script','weight')
        widgets = {
            'page_title': TextInput(attrs={'data-length':60}),
            'description': Textarea(attrs={'data-length':160})
        }

class CustomForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(CustomForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        if instance.web_id:
            #self.fields["category"].queryset = Node.objects.filter(node_type="c", web_id=instance.web_id)
            languages =list()
            for x in Translation.objects.filter(web_id=instance.web_id).values('id','language'):
                e = [x["language"],x["language"]]
                languages.append(e)

            self.fields["language"].choices = languages

    def clean_body(self):
        valid_characters = string.printable
        data = self.cleaned_data['body']
        start_string = data
        end_string = ''.join(i for i in start_string if i in valid_characters)

        return end_string;

    class Meta:
        model = Node
        fields = ('title','language','summary','body','in_menu')
        widgets = {
            'summary': Textarea(),
            'language': Select(),
        }

class TourForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(TourForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        if instance.web_id:
            self.fields["category"].queryset = Node.objects.filter(node_type="c", web_id=instance.web_id)
            languages =list()
            for x in Translation.objects.filter(web_id=instance.web_id).values('id','language'):
                e = [x["language"],x["language"]]
                languages.append(e)

            self.fields["language"].choices = languages

    def clean_body(self):
        valid_characters = string.printable
        data = self.cleaned_data['body']
        start_string = data
        end_string = ''.join(i for i in start_string if i in valid_characters)

        return end_string;

    class Meta:
        model = Node
        fields = ('title', 'language', 'money', 'price','quantity', 'summary', 'body', 'include', 'recommendations', 'in_menu', 'category','promoted','front')
        widgets = {
            'summary': Textarea(),
            'language': Select(),
        }

class RoomForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RoomForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        if instance.web_id:
            self.fields["category"].queryset = Node.objects.filter(node_type="c", web_id=instance.web_id)
            languages =list()
            for x in Translation.objects.filter(web_id=instance.web_id).values('id','language'):
                e = [x["language"],x["language"]]
                languages.append(e)

            self.fields["language"].choices = languages

    class Meta:
        model = Node
        fields = ('title', 'language', 'money', 'price','quantity', 'summary', 'body', 'include', 'recommendations', 'category','promoted','front')
        widgets = {
            'summary': Textarea(),
            'language': Select(),
        }
        labels = {
            'quantity': u'Cantidad de camas',
        }

class LandingForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(LandingForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        if instance.web_id:
            self.fields["category"].queryset = Node.objects.filter(node_type="c", web_id=instance.web_id)
            languages =list()
            for x in Translation.objects.filter(web_id=instance.web_id).values('id','language'):
                e = [x["language"],x["language"]]
                languages.append(e)

            self.fields["language"].choices = languages

    """
    def clean_body(self):
        data = self.cleaned_data['body']

        clean_data = BeautifulSoup(data, 'html.parser')
        for match in clean_data.findAll('span'):
            match.unwrap()

        return clean_data;
    """

    class Meta:
        model = Node
        fields = ('title','language','summary','body','include','recommendations','category','in_menu','promoted','front')
        widgets = {
            'summary': Textarea(),
            'language': Select(),
        }
