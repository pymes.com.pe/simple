# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from business.models import Web
from cms.models import Translation, Appearance
from cms.forms import TranslationForm, TranslationEditForm, TranslationSeoForm, StructureForm


@login_required()
def clear_cache(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.warning(request, "Dominio no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    ctx = {
        "web_pk":web_pk,
        "web_domain":web.domain,
        "web_protocol":web.protocol,
        'obj': web,
        }
    return render(request,'domains/clear_cache.html',ctx)


@login_required()
def dashboard(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.warning(request, "Dominio no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))
    appearance = Appearance.objects.get(pk=web_pk)
    translations = Translation.objects.filter(web_id=web_pk)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web.domain,
        "web_protocol":web.protocol,
        "appearance":appearance,
        "translations":translations,
        "obj":web,
    }
    return render(request,'domains/dashboard.html',ctx)

@login_required()
def detail(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Translation.objects.get(pk=pk, web_id=web_pk)
    except Translation.DoesNotExist:
        obj = Translation.objects.create(pk=web_pk)

    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        'obj': obj,
        }
    return render(request,'cms/translation_detail.html',ctx)

@login_required()
def ci(request, web_pk, pk, ci_origin):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Translation.objects.get(pk=pk, web_id=web_pk)
    except Translation.DoesNotExist:
        # obj = Translation.objects.create(pk=web_pk)
        pass

    try:
        img = Translation.objects.get(pk=ci_origin, web_id=web_pk)
        obj.image = img.image
        obj.save()
    except Translation.DoesNotExist:
        # img = Translation.objects.create(pk=web_pk)
        pass
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        'obj': obj,
        }
    return render(request,'cms/translation_detail.html',ctx)


@login_required()
def update(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Translation.objects.get(pk=pk, web_id=web_pk)
    except Translation.DoesNotExist:
        obj = Translation.objects.create(pk=web_pk)

    if request.method == 'POST':
        form = TranslationEditForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Información actualizado.")
            return HttpResponseRedirect(reverse("translations_detail", args=(web_pk, pk)))
        else:
            messages.warning(request, 'Verifique la información completada.')
    else:
        form = TranslationEditForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        'form': form,
        }
    return render(request,'cms/translation_form_a.html',ctx)

@login_required()
def structure(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Translation.objects.get(pk=pk, web_id=web_pk)
    except Translation.DoesNotExist:
        obj = Translation.objects.create(pk=web_pk)

    if request.method == 'POST':
        form = StructureForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Información de estructura actualizado.")
            return HttpResponseRedirect(reverse("translations_detail", args=(web_pk, pk)))
        else:
            messages.warning(request, 'Verifique la información completada.')
    else:
        form = StructureForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        'form': form,
        }
    return render(request,'cms/translation_form.html',ctx)

@login_required()
def seo(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Translation.objects.get(pk=pk)
    except Translation.DoesNotExist:
        obj = Translation.objects.create(pk=pk)

    if request.method == 'POST':
        form = TranslationSeoForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Información actualizado.")
            return HttpResponseRedirect(reverse("translations_detail", args=(web_pk, pk)))
        else:
            messages.warning(request, 'Verifique la información completada.')
    else:
        form = TranslationSeoForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        'form': form,
        }
    return render(request,'nodes/seo.html',ctx)


@login_required
def new(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","multi_language").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","multi_language").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    if not web["multi_language"]:
        messages.info(request, "Página no es el multi idioma.")
        return HttpResponseRedirect(reverse("webs_dashboard", args=(web_pk,)))

    if request.method == 'POST':
        language = request.POST.get("language")
        if Translation.objects.filter(web_id=web_pk, language=language).exists():
            messages.info(request, "Lenguaje ya exíste.")
            return HttpResponseRedirect(reverse("translations_new", args=(web_pk,)))

        obj = Translation(web_id=web_pk)
        form = TranslationForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Información actualizado.")
            return HttpResponseRedirect(reverse("webs_dashboard", args=(web_pk,)))
        else:
            messages.warning(request, 'Verifique la información completada.')
    else:
        form = TranslationForm()
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        'form': form,
        }
    return render(request,'cms/translation_form_a.html',ctx)