from django.shortcuts import render
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from business.models import Web
from cms.models import Translation, Node, Menu
from cms.nodes_forms import ContentForm, ContentSeoForm, PageForm, CustomForm, TourForm, RoomForm, LandingForm

@login_required
def create(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol","name").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol","name").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    if request.method == "POST":
        obj = Node(web_id=web_pk)
        obj.page_title = "%s | %s" % (request.POST.get('title'), web['name'])
        form = ContentForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            new = form.save()
            if not new.id:
                messages.success(request, "Contenido YA EXISTE")
            else:
                messages.success(request, "Contenido creado")
                return HttpResponseRedirect(reverse('nodes_detail', args=(web_pk, new)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        obj = Node(web_id=web_pk)
        form = ContentForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"nodes/create.html", ctx)


@login_required()
def page(request, web_pk, content_type='p'):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol","name").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol","name").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    if request.method == "POST":
        obj = Node(web_id=web_pk, node_type=content_type, with_image=False)
        obj.page_title = "%s | %s" % (request.POST.get('title'), web['name'])
        form = PageForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            new = form.save()
            if not new.id:
                messages.success(request, "Contenido YA EXISTE")
            else:
                messages.success(request, "Contenido creado")
                return HttpResponseRedirect(reverse('nodes_detail', args=(web_pk, new.pk)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        obj = Node(web_id=web_pk)
        form = PageForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"nodes/create.html", ctx)


@login_required()
def form(request, web_pk, content_type='fc'):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol","name").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol","name").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    if request.method == "POST":
        obj = Node(web_id=web_pk, node_type=content_type, with_image=False)
        obj.page_title = "%s | %s" % (request.POST.get('title'), web['name'])
        form = CustomForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            new = form.save()
            if not new.id:
                messages.success(request, "Contenido YA EXISTE")
            else:
                messages.success(request, "Contenido creado")
                return HttpResponseRedirect(reverse('nodes_detail', args=(web_pk, new.pk)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        obj = Node(web_id=web_pk, with_image=False)
        form = CustomForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"nodes/create.html", ctx)

@login_required()
def tour(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol","name").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol","name").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    if request.method == "POST":
        obj = Node(web_id=web_pk, node_type="t")
        obj.page_title = "%s | %s" % (request.POST.get('title'), web['name'])
        form = TourForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            new = form.save()
            if not new.id:
                messages.success(request, "Contenido YA EXISTE")
            else:
                if new.in_menu:
                    try:
                        menu = Menu.objects.get(node_id=new.pk)
                        menu.language = new.language
                        menu.slug = new.slug
                        menu.save()
                    except Menu.DoesNotExist:
                        Menu.objects.create(
                            node_id=new.pk,
                            title=new.title,
                            language=new.language,
                            slug=new.slug,
                            web_id=new.web_id,
                            )
                messages.success(request, "Tour creado")
                return HttpResponseRedirect(reverse('nodes_detail', args=(web_pk,new.pk)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        obj = Node(web_id=web_pk)
        form = TourForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"nodes/create.html", ctx)

@login_required
def tour_edit(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))
    try:
        obj = Node.objects.get(pk=pk, web_id=web_pk)
    except Node.DoesNotExist:
        messages.info(request, "Contenido no encontrado")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))

    if request.method == "POST":
        form = TourForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            node = form.save()
            if node.in_menu:
                try:
                    menu = Menu.objects.get(node_id=pk)
                    menu.language = node.language
                    menu.slug = node.slug
                    menu.save()
                except Menu.DoesNotExist:
                    Menu.objects.create(
                        node_id=node.pk,
                        title=node.title,
                        language=node.language,
                        slug=node.slug,
                        web_id=node.web_id,
                        )
            messages.success(request, "Tour actualizado")
            return HttpResponseRedirect(reverse('nodes_detail', args=(web_pk, pk)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = TourForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"nodes/edit.html", ctx)


@login_required
def room(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol","name").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol","name").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    if request.method == "POST":
        obj = Node(web_id=web_pk, node_type="r")
        obj.page_title = "%s | %s" % (request.POST.get('title'), web['name'])
        form = RoomForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            new = form.save()
            if not new.id:
                messages.success(request, "Contenido YA EXISTE")
            else:
                messages.success(request, "Contenido creado")
                return HttpResponseRedirect(reverse('nodes_detail', args=(web_pk, new.pk)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        obj = Node(web_id=web_pk)
        form = RoomForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"nodes/create.html", ctx)


@login_required()
def room_edit(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))
    try:
        obj = Node.objects.get(pk=pk, web_id=web_pk)
    except Node.DoesNotExist:
        messages.info(request, "Contenido no encontrado")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))

    if request.method == "POST":
        form = RoomForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            messages.success(request, "Contenido actualizado")
            return HttpResponseRedirect(reverse('nodes_detail', args=(web_pk, pk)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = RoomForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"nodes/edit.html", ctx)




@login_required()
def landing(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol","name").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol","name").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    if request.method == "POST":
        obj = Node(web_id=web_pk, node_type='l', with_image=False)
        obj.page_title = "%s | %s" % (request.POST.get('title'), web['name'])
        form = LandingForm(request.POST, instance=obj)
        if form.is_valid():
            new = form.save()
            if not new.id:
                messages.success(request, "Contenido YA EXISTE")
            else:
                messages.success(request, "Contenido creado")
                return HttpResponseRedirect(reverse('nodes_detail', args=(web_pk, new.pk)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        obj = Node(web_id=web_pk)
        form = LandingForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"nodes/landing.html", ctx)
