from django.shortcuts import render
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from business.models import Web
from webform.models import Contact, BookTour, BookRoom

@login_required
def index(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    objs_list = Contact.objects.filter(web_id=web_pk)
    paginator = Paginator(objs_list, 16)
    page = request.GET.get('page')

    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "objs":objs,
    }
    return render(request,"webforms/index.html", ctx)

@login_required
def detail(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))
    try:
        obj = Contact.objects.get(pk=pk, web_id=web_pk)
    except Contact.DoesNotExist:
        messages.info(request, "Mensaje no encontrado.")
        return HttpResponseRedirect(reverse("webforms", args=(web_pk,)))
    if not obj.read:
        obj.read = True
        obj.save()
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "obj":obj,
    }
    return render(request,"webforms/detail.html", ctx)

@login_required
def delete(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Contact.objects.get(pk=pk, web_id=web_pk)
    except Contact.DoesNotExist:
        messages.warning(request, "Mensaje no encontrado.")
        return HttpResponseRedirect(reverse("webforms", args=(web_pk,)))
    if request.method == "POST":
        obj.delete()
        messages.warning(request, "Mensaje eliminado.")
        return HttpResponseRedirect(reverse("webforms", args=(web_pk,)))

    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
    }
    return render(request,"webforms/delete.html", ctx)


@login_required
def rooms(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    objs_list = BookRoom.objects.filter(web_id=web_pk)
    paginator = Paginator(objs_list, 16)
    page = request.GET.get('page')

    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "objs":objs,
    }
    return render(request,"webforms/room_list.html", ctx)


@login_required
def rooms_detail(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))
    try:
        obj = BookRoom.objects.get(pk=pk, web_id=web_pk)
    except BookRoom.DoesNotExist:
        messages.info(request, "Reserva no encontrado.")
        return HttpResponseRedirect(reverse("webforms_rooms", args=(web_pk,)))
    if not obj.read:
        obj.read = True
        obj.save()
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "obj":obj,
    }
    return render(request,"webforms/room_detail.html", ctx)


@login_required
def tours(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    objs_list = BookTour.objects.filter(web_id=web_pk)
    paginator = Paginator(objs_list, 16)
    page = request.GET.get('page')

    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)

    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "objs":objs,
    }
    return render(request,"webforms/tour_list.html", ctx)

@login_required
def tours_detail(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))
    try:
        obj = BookTour.objects.get(pk=pk, web_id=web_pk)
    except BookTour.DoesNotExist:
        messages.info(request, "Reserva no encontrado.")
        return HttpResponseRedirect(reverse("webforms_tours", args=(web_pk,)))
    if not obj.read:
        obj.read = True
        obj.save()
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "obj":obj,
    }
    return render(request,"webforms/tour_detail.html", ctx)
