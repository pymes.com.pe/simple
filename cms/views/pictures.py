from django.shortcuts import render
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from business.models import Web
from cms.models import Picture, Node
from cms.forms import PictureForm

@login_required
def index(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    objs_list = Picture.objects.filter(web_id=web_pk)
    paginator = Paginator(objs_list, 25)
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "objs":objs,
    }
    return render(request,"pictures/index.html", ctx)

@login_required
def  detail(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Picture.objects.get(pk=pk, web_id=web_pk)
    except Picture.DoesNotExist:
        messages.warning(request, "Imagen no encontrado.")
        return HttpResponseRedirect(reverse("pictures", args=(web_pk,)))
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "obj":obj,
    }
    return render(request,"pictures/detail.html", ctx)

@login_required
def edit(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Picture.objects.get(pk=pk, web_id=web_pk)
    except Picture.DoesNotExist:
        messages.warning(request, "Imagen no encontrado.")
        return HttpResponseRedirect(reverse("pictures", args=(web_pk,)))

    if request.method == "POST":
        form = PictureForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request,"Imagen modificado")
            if request.GET.get('node_id'):
                return HttpResponseRedirect(reverse("nodes_detail", args=(web_pk, request.GET.get('node_id'))))
            else:
                return HttpResponseRedirect(reverse("pictures", args=(web_pk,)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = PictureForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
        "pk":obj.pk,
    }
    return render(request,"pictures/edit.html", ctx)

@login_required
def new(request, web_pk, page_pk=0):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    if request.method == "POST":
        obj = Picture(web_id=web_pk)
        form = PictureForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            picture = form.save()
            messages.success(request, "Imagen creado")
            if page_pk != 0:
                try:
                    node = Node.objects.get(pk=page_pk)
                except Node.DoesNotExist:
                    return HttpResponseRedirect(reverse("pictures", args=(web_pk,)))
                node.images.add(picture)
                return HttpResponseRedirect(reverse("nodes_detail", args=(web_pk,page_pk,)))
            else:
                return HttpResponseRedirect(reverse("pictures", args=(web_pk,)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = PictureForm()
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"pictures/new.html", ctx)

@login_required()
def delete(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Picture.objects.get(pk=pk, web_id=web_pk)
    except Picture.DoesNotExist:
        messages.warning(request, "Imagen no encontrado.")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))
    if request.method == "POST":
        obj.delete()
        messages.warning(request, "Imagen eliminado.")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))

    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
    }
    return render(request,"pictures/delete.html", ctx)

@login_required()
def change_img(request, web_pk, pk, image_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Picture.objects.get(pk=pk, web_id=web_pk)
    except Picture.DoesNotExist:
        messages.warning(request, "Imagen no encontrado.")

    try:
        new = Picture.objects.get(pk=image_pk, web_id=web_pk)
    except Picture.DoesNotExist:
        messages.warning(request, "Imagen no encontrado.")
    obj.image = new.image
    obj.save()

    return HttpResponseRedirect(reverse("pictures", args=(web_pk,)))
