from django.shortcuts import render
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from business.models import Web
from common.models import Redirect
from common.forms import RedirectForm

@login_required
def index(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    objs = Redirect.objects.filter(web_id=web_pk)
    form = RedirectForm()
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "objs":objs,
        "form":form,
    }
    return render(request,"redirects/index.html", ctx)

@login_required
def edit(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Redirect.objects.get(pk=pk, web_id=web_pk)
    except Redirect.DoesNotExist:
        messages.warning(request, "Redirect no encontrado.")
        return HttpResponseRedirect(reverse("redirects", args=(web_pk,)))
    if request.method == "POST":
        form = RedirectForm(request.POST, instance=obj)
        if form.is_valid():
            new = form.save()
            messages.success(request, "Redirect actualizado")
            return HttpResponseRedirect(reverse("redirects", args=(web_pk,)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = RedirectForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
        "pk":pk,
    }
    return render(request,"redirects/edit.html", ctx)

@login_required
def delete(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Redirect.objects.get(pk=pk, web_id=web_pk)
    except Redirect.DoesNotExist:
        messages.warning(request, "Redirect no encontrado.")
        return HttpResponseRedirect(reverse("redirects", args=(web_pk,)))
    if request.method == "POST":
        obj.delete()
        messages.warning(request, "Bloque eliminado.")
        return HttpResponseRedirect(reverse("blocks", args=(web_pk,)))

    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
    }
    return render(request,"blocks/delete.html", ctx)

@login_required
def new(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    if request.method == "POST":
        obj = Redirect(web_id=web_pk)
        form = RedirectForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Redirect agregado")
            return HttpResponseRedirect(reverse("redirects", args=(web_pk,)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = RedirectForm()
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"redirects/new.html", ctx)