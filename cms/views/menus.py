from django.shortcuts import render
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from business.models import Web
from cms.models import Menu, Node
from cms.forms import MenuForm

@login_required
def index(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    language = request.GET.get('language', 'es')

    objs_list = Menu.objects.filter(web_id=web_pk, parent__isnull=True, language=language)
    menu = []
    for o in objs_list:
        submenu = []
        num_elements = 0
        for subitem in o.menu_set.all():
            sub = {
                "pk": subitem.pk,
                "title": subitem.title,
                "language": subitem.language,
                "parent": subitem.parent_id,
                "node_id": subitem.node_id
            }
            submenu.append(sub)
            num_elements = num_elements + 1
        e = {
            "pk": o.pk,
            "title": o.title,
            "weight": o.weight,
            "language": o.language,
            "submenu": submenu,
            "node_id": o.node_id
        }
        menu.append(e)

    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "objs":menu,
    }
    return render(request,"menus/index.html", ctx)

@login_required
def edit(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Menu.objects.get(pk=pk, web_id=web_pk)
    except Menu.DoesNotExist:
        messages.warning(request, "Menu no encontrado.")
        return HttpResponseRedirect(reverse("menus", args=(web_pk,)))

    if request.method == "POST":
        form = MenuForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Item de menu actualizado")
            return HttpResponseRedirect(reverse("menus", args=(web_pk,)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = MenuForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
        "pk":pk,
    }
    return render(request,"menus/edit.html", ctx)


@login_required
def delete(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Menu.objects.get(pk=pk, web_id=web_pk)
    except Menu.DoesNotExist:
        messages.warning(request, "Menu no encontrado.")
        return HttpResponseRedirect(reverse("menus", args=(web_pk,)))

    if request.method == "POST":
        if obj.node_id:
            node = Node.objects.get(pk=obj.node_id)
            node.in_menu = False
            node.save()
        obj.delete()
        messages.warning(request, "Item de menu eliminado.")
        return HttpResponseRedirect(reverse("menus", args=(web_pk,)))

    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
    }
    return render(request,"menus/delete.html", ctx)


@login_required
def new(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    obj = Menu(web_id = web_pk)
    if request.method == "POST":
        form = MenuForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Item de menu creado")
            return HttpResponseRedirect(reverse("menus", args=(web_pk,)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = MenuForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form
    }
    return render(request,"menus/create.html", ctx)