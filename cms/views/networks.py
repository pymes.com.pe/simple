from django.shortcuts import render
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from business.models import Web
from cms.models import Network
from cms.forms import NetworkForm

@login_required
def index(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    objs = Network.objects.filter(web_id=web_pk)
    form = NetworkForm()
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "objs":objs,
        "form":form,
    }
    return render(request,"networks/index.html", ctx)

@login_required
def edit(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Network.objects.get(pk=pk, web_id=web_pk)
    except Network.DoesNotExist:
        messages.warning(request, "Red social no encontrado.")
        return HttpResponseRedirect(reverse("networks", args=(web_pk,)))

    if request.method == "POST":
        form = NetworkForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Red social actualizado")
            return HttpResponseRedirect(reverse("networks", args=(web_pk,)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = NetworkForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
        "pk":pk,
    }
    return render(request,"networks/edit.html", ctx)

@login_required
def delete(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Network.objects.get(pk=pk, web_id=web_pk)
    except Network.DoesNotExist:
        messages.warning(request, "Red social no encontrado.")
        return HttpResponseRedirect(reverse("networks", args=(web_pk,)))
    if request.method == "POST":
        obj.delete()
        messages.warning(request, "Red social eliminado.")
        return HttpResponseRedirect(reverse("networks", args=(web_pk,)))

    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
    }
    return render(request,"networks/delete.html", ctx)

@login_required
def new(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    if request.method == "POST":
        obj = Network(web_id=web_pk)
        form = NetworkForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Red social agregado")
            return HttpResponseRedirect(reverse("networks", args=(web_pk,)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = NetworkForm()
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"networks/new.html", ctx)