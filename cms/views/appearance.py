# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from business.models import Web
from cms.models import Appearance
from cms.forms import CssJsForm, WidgetForm


@login_required
def css_js(request,web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Appearance.objects.get(pk=web_pk)
    except Appearance.DoesNotExist:
        obj = Appearance.objects.create(pk=web_pk)

    if request.method == 'POST':
        form = CssJsForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Información actualizado.")
            return HttpResponseRedirect(reverse("webs_dashboard", args=(web_pk,)))
        else:
            messages.warning(request, 'Verifique la información completada.')
    else:
        form = CssJsForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        'form': form,
        }
    return render(request,'domains/css_js.html',ctx)


@login_required
def widget(request,web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))
    try:
        obj = Appearance.objects.get(pk=web_pk)
    except Appearance.DoesNotExist:
        obj = Appearance.objects.create(web_id=web_pk)

    if request.method == 'POST':
        form = WidgetForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Información actualizado.")
            return HttpResponseRedirect(reverse("webs_dashboard", args=(web_pk,)))
        else:
            messages.warning(request, 'Verifique la información completada.')
    else:
        form = WidgetForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        'form': form,
        }
    return render(request,'domains/widget.html',ctx)