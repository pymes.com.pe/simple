from django.shortcuts import render
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from business.models import Web
from cms.models import Block
from cms.forms import BlockForm

@login_required
def index(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    objs = Block.objects.filter(web_id=web_pk)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "objs":objs,
    }
    return render(request,"blocks/index.html", ctx)

@login_required
def detail(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Block.objects.get(pk=pk, web_id=web_pk)
    except Block.DoesNotExist:
        messages.warning(request, "Block no encontrado.")
        return HttpResponseRedirect(reverse("blocks", args=(web_pk,)))

    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "obj":obj,
    }
    return render(request,"blocks/detail.html", ctx)

@login_required
def edit(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Block.objects.get(pk=pk, web_id=web_pk)
    except Block.DoesNotExist:
        messages.warning(request, "Block no encontrado.")
        return HttpResponseRedirect(reverse("blocks", args=(web_pk,)))
    if request.method == "POST":
        form = BlockForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            new = form.save()
            messages.success(request, "Block actualizado")
            ctx = {
                "web_pk":web_pk,
                "web_domain":web["domain"],
        "web_protocol":web["protocol"],
                "obj":new,
            }
            return render(request,'blocks/detail.html',ctx)
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = BlockForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"blocks/edit.html", ctx)

@login_required
def delete(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Block.objects.get(pk=pk, web_id=web_pk)
    except Block.DoesNotExist:
        messages.warning(request, "Block no encontrado.")
        return HttpResponseRedirect(reverse("blocks", args=(web_pk,)))
    if request.method == "POST":
        obj.delete()
        messages.warning(request, "Block eliminado.")
        return HttpResponseRedirect(reverse("blocks", args=(web_pk,)))

    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
    }
    return render(request,"blocks/delete.html", ctx)

@login_required
def new(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    if request.method == "POST":
        obj = Block(web_id=web_pk)
        form = BlockForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Block agregado")
            return HttpResponseRedirect(reverse("blocks", args=(web_pk,)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = BlockForm()
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"blocks/new.html", ctx)