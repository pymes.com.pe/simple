from django.shortcuts import render
from django.urls import reverse
from django.http.response import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from business.models import Web
from cms.models import Translation, Node, Menu, Appearance, Picture
from cms.nodes_forms import ContentForm, ContentSeoForm, PageForm, ContentAllForm, ImageForm

@login_required
def index(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    search = request.GET.get('search', None)
    fronts = None
    if search:
        objs_list = Node.objects.filter(web_id=web_pk, title__icontains=search)
    else:
        search = ""
        objs_list = Node.objects.filter(web_id=web_pk)

    paginator = Paginator(objs_list, 10)
    page = request.GET.get('page')

    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
        if not search:
            fronts = Translation.objects.filter(web_id=web_pk)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "objs":objs,
        "fronts": fronts,
        "search": search,
    }
    return render(request,"nodes/index.html", ctx)

@login_required
def  detail(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Node.objects.get(pk=pk, web_id=web_pk)
    except Node.DoesNotExist:
        messages.warning(request, "Contenido no encontrado.")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "obj":obj,
    }
    if obj.node_type == 't':
        return render(request,"nodes/tours.html", ctx)
    else:
        return render(request,"nodes/detail.html", ctx)


@login_required
def edit(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Node.objects.get(pk=pk, web_id=web_pk)
    except Node.DoesNotExist:
        messages.warning(request, "Contenido no encontrado.")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))

    if request.method == "POST":
        form = ContentForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            node = form.save()
            if node.in_menu:
                try:
                    menu = Menu.objects.get(node_id=pk)
                    menu.language = node.language
                    menu.slug = node.slug
                    menu.save()
                except Menu.DoesNotExist:
                    Menu.objects.create(
                        node_id=node.pk,
                        title=node.title,
                        language=node.language,
                        slug=node.slug,
                        web_id=node.web_id,
                        )
            messages.success(request, "Contenido actualizado")
            ctx = {
                "web_pk":web_pk,
                "web_domain":web["domain"],
                "web_protocol":web["protocol"],
                "obj":node,
            }
            return render(request,'nodes/detail.html',ctx)
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = ContentForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
        "pk":obj.pk,
    }
    return render(request,"nodes/edit.html", ctx)


@login_required()
def image(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Node.objects.get(pk=pk, web_id=web_pk)
    except Node.DoesNotExist:
        messages.warning(request, "Contenido no encontrado.")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))

    if request.method == "POST":
        form = ImageForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            form.save()
            messages.info(request, 'Imagen actualizado')
            return HttpResponseRedirect(reverse('nodes_detail', args=(web_pk, pk)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = ImageForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
        "pk":obj.pk,
    }
    return render(request,"nodes/form.html", ctx)


@login_required()
def ci(request, web_pk, pk, node_origin):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Node.objects.get(pk=pk, web_id=web_pk)
    except Node.DoesNotExist:
        messages.warning(request, "Contenido no encontrado.")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))

    try:
        img = Node.objects.get(pk=node_origin, web_id=web_pk)
    except Node.DoesNotExist:
        messages.warning(request, "Contenido no encontrado.")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))
    obj.image = img.image
    obj.save()
    messages.warning(request, "Imagen Actualizado.")
    return HttpResponseRedirect(reverse("nodes_detail", args=(web_pk,pk)))


@login_required()
def ni(request, web_pk, pk, node_origin):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Node.objects.get(pk=pk, web_id=web_pk)
    except Node.DoesNotExist:
        messages.warning(request, "Contenido no encontrado.")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))

    try:
        img = Node.objects.get(pk=node_origin, web_id=web_pk)
    except Node.DoesNotExist:
        messages.warning(request, "Contenido no encontrado.")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))
    p = Picture()
    p.image = img.image
    p.alt = img.title
    p.web_id = web_pk
    p.save()
    obj.images.add(p)
    messages.warning(request, "Imagen creado.")
    return HttpResponseRedirect(reverse("nodes_detail", args=(web_pk,pk)))



@login_required()
def cp(request, web_pk, pk, picture_id):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Node.objects.get(pk=pk, web_id=web_pk)
    except Node.DoesNotExist:
        messages.warning(request, "Contenido no encontrado.")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))

    try:
        img = Picture.objects.get(pk=picture_id, web_id=web_pk)
    except Picture.DoesNotExist:
        messages.warning(request, "Imagen no encontrado.")
        return HttpResponseRedirect(reverse("nodes_detail", args=(web_pk, pk)))
    obj.image = img.image
    obj.save()
    messages.warning(request, "Imagen creado.")
    return HttpResponseRedirect(reverse("nodes_detail", args=(web_pk, pk)))

@login_required
def clonar(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Node.objects.get(pk=pk, web_id=web_pk)
    except Node.DoesNotExist:
        messages.warning(request, "Contenido no encontrado.")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))

    new = Node(
        money = obj.money,
        price = obj.price,
        quantity = obj.quantity,
        with_image = obj.with_image,
        image = obj.image,
        language = obj.language,
        promoted = obj.promoted,
        front = obj.front,
        node_type = obj.node_type,
        category = obj.category,
        status = obj.status,
        web_id = obj.web_id,
        weight = obj.weight,
        in_menu = obj.in_menu,
        robot_index = obj.robot_index,
        amp = obj.amp
        )
    if request.method == "POST":
        form = ContentAllForm(request.POST, request.FILES, instance=new)
        if form.is_valid():
            node = form.save()
            messages.success(request, "Contenido creado")
            return HttpResponseRedirect(reverse('nodes_detail',args=(web_pk, node.pk)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = ContentAllForm(instance=new)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"nodes/clonar.html", ctx)

@login_required
def seo(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Node.objects.get(pk=pk, web_id=web_pk)
    except Node.DoesNotExist:
        messages.warning(request, "Contenido no encontrado.")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))
    if request.method == "POST":
        form = ContentSeoForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Contenido actualizado")
            return HttpResponseRedirect(reverse("nodes_detail", args=(web_pk,pk)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = ContentSeoForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"nodes/seo.html", ctx)


@login_required
def new(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))
    content_type = Web.objects.values('template_type').get(pk=web_pk)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "template_type":content_type["template_type"],
    }
    return render(request,"nodes/new.html", ctx)


@login_required
def delete(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Node.objects.get(pk=pk, web_id=web_pk)
    except Node.DoesNotExist:
        messages.warning(request, "Contenido no encontrado.")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))
    if request.method == "POST":
        obj.delete()
        messages.warning(request, "Contenido eliminado.")
        return HttpResponseRedirect(reverse("nodes", args=(web_pk,)))

    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
    }
    return render(request,"nodes/delete.html", ctx)