from django.shortcuts import render
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from business.models import Web
from cms.models import Slider
from cms.forms import SliderForm

@login_required
def index(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    objs = Slider.objects.filter(web_id=web_pk)

    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "objs":objs,
    }
    return render(request,"sliders/index.html", ctx)

@login_required
def  detail(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Slider.objects.get(pk=pk, web_id=web_pk)
    except Slider.DoesNotExist:
        messages.warning(request, "Slider no encontrado.")
        return HttpResponseRedirect(reverse("sliders", args=(web_pk,)))
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "obj":obj,
    }
    return render(request,"sliders/detail.html", ctx)

@login_required
def edit(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Slider.objects.get(pk=pk, web_id=web_pk)
    except Slider.DoesNotExist:
        messages.warning(request, "Slider no encontrado.")
        return HttpResponseRedirect(reverse("sliders", args=(web_pk,)))
    if request.method == "POST":
        form = SliderForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            new = form.save()
            messages.success(request, "Slider actualizado")
            return HttpResponseRedirect(reverse("sliders", args=(web_pk,)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = SliderForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"sliders/edit.html", ctx)

@login_required
def clone(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Slider.objects.get(pk=pk, web_id=web_pk)
    except Slider.DoesNotExist:
        messages.warning(request, "Slider no encontrado.")
        return HttpResponseRedirect(reverse("sliders", args=(web_pk,)))
    new = Slider(
        title = obj.title,
        body = obj.body,
        language = obj.language,
        link_title = obj.link_title,
        link_url = obj.link_url,
        image = obj.image,
        weight = obj.weight,
        status = obj.status,
        web_id = web_pk
        )
    if request.method == "POST":
        form = SliderForm(request.POST, request.FILES, instance=new)
        if form.is_valid():
            form.save()
            messages.success(request, "Slider Creado")
            return HttpResponseRedirect(reverse("sliders", args=(web_pk,)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = SliderForm(instance=new)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"sliders/edit.html", ctx)


@login_required
def new(request, web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    obj = Slider(web_id=web_pk)
    if request.method == "POST":
        form = SliderForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Slider creado")
            return HttpResponseRedirect(reverse("sliders", args=(web_pk,)))
        else:
            messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = SliderForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
        "form":form,
    }
    return render(request,"sliders/new.html", ctx)

@login_required
def delete(request, web_pk, pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.values("domain","protocol").get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.values("domain","protocol").get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    try:
        obj = Slider.objects.get(pk=pk, web_id=web_pk)
    except Slider.DoesNotExist:
        messages.warning(request, "Slider no encontrado.")
        return HttpResponseRedirect(reverse("sliders", args=(web_pk,)))
    if request.method == "POST":
        obj.delete()
        messages.warning(request, "Slider eliminado.")
        return HttpResponseRedirect(reverse("sliders", args=(web_pk,)))

    ctx = {
        "web_pk":web_pk,
        "web_domain":web["domain"],
        "web_protocol":web["protocol"],
    }
    return render(request,"sliders/delete.html", ctx)