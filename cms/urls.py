from django.conf.urls import url
from cms.views import translations, appearance, nodes, contents, networks, networks
from cms.views import sliders, menus, blocks, webforms, pictures, redirects, reports, documents
urlpatterns = [
    # fronts
    url(r'^$', translations.dashboard, name='webs_dashboard'),
    url(r'^clear_cache/$', translations.clear_cache, name='clear_cache'),
    # Idiomas
    url(r'^translations/(?P<pk>\d+)/$', translations.detail, name='translations_detail'),
    url(r'^translations/(?P<pk>\d+)/update$', translations.update, name='translations_update'),
    url(r'^translations/(?P<pk>\d+)/structure$', translations.structure, name='translations_structure'),
    url(r'^translations/(?P<pk>\d+)/seo$', translations.seo, name='translations_seo'),
    url(r'^translations/(?P<pk>\d+)/ci/(?P<ci_origin>\d+)/$', translations.ci, name='translations_ci'),
    url(r'^translations/new$', translations.new, name='translations_new'),
    # Appearance
    url(r'^template/css_js$', appearance.css_js, name='webs_css_js'),
    url(r'^template/widget$', appearance.widget, name='webs_widget'),
    # nodes
    url(r'^nodes/$', nodes.index, name='nodes'),
    url(r'^nodes/(?P<pk>\d+)/$', nodes.detail, name='nodes_detail'),
    url(r'^nodes/(?P<pk>\d+)/edit$', nodes.edit, name='nodes_edit'),
    url(r'^nodes/(?P<pk>\d+)/clonar$', nodes.clonar, name='nodes_clonar'),
    url(r'^nodes/(?P<pk>\d+)/seo$', nodes.seo, name='nodes_seo'),
    url(r'^nodes/(?P<pk>\d+)/delete$', nodes.delete, name='nodes_delete'),
    url(r'^nodes/(?P<pk>\d+)/image$', nodes.image, name='nodes_image'),
    url(r'^nodes/(?P<pk>\d+)/ci/(?P<node_origin>\d+)/$', nodes.ci, name='nodes_ci'),# copy img node->node
    url(r'^nodes/(?P<pk>\d+)/ni/(?P<node_origin>\d+)/$', nodes.ni, name='nodes_ni'),# new img node->node
    url(r'^nodes/(?P<pk>\d+)/cp/(?P<picture_id>\d+)/$', nodes.cp, name='nodes_cp'), # copy img picture->node
    url('^nodes/new$', nodes.new, name='nodes_new'),
    # Forms for especified content type
    url(r'^nodes/create/landing$', contents.landing, name='nodes_landing'),
    url(r'^nodes/create/page/(?P<content_type>\w{1})$', contents.page, name='nodes_page'),
    url(r'^nodes/create/froms/(?P<content_type>\w{2})$', contents.form, name='nodes_form'),

    url('^nodes/create/tour$', contents.tour, name='nodes_tour'),
    url(r'^nodes/tour/(?P<pk>\d+)/update$', contents.tour_edit, name='nodes_tour_edit'),
    url('^nodes/create/room$', contents.room, name='nodes_room'),
    url(r'^nodes/room/(?P<pk>\d+)/update$', contents.room_edit, name='nodes_room_edit'),
    # Pictures
    url(r'^pictures/$', pictures.index, name='pictures'),
    url(r'^pictures/(?P<pk>\d+)/$', pictures.detail, name='pictures_detail'),
    url(r'^pictures/(?P<pk>\d+)/edit$', pictures.edit, name='pictures_edit'),
    url(r'^pictures/(?P<pk>\d+)/delete$', pictures.delete, name='pictures_delete'),
    url(r'^pictures/(?P<page_pk>\d+)/new$', pictures.new, name='pictures_new'),
    url(r'^pictures/(?P<pk>\d+)/change_image/(?P<image_pk>\d+)/$', pictures.change_img, name='pictures_change'),
    url(r'^pictures/new$', pictures.new, name='pictures_new'),
    # documents
    url(r'^documents/$', documents.index, name='documents'),
    url(r'^documents/(?P<pk>\d+)/edit$', documents.edit, name='documents_edit'),
    url(r'^documents/(?P<pk>\d+)/delete$', documents.delete, name='documents_delete'),
    url(r'^documents/(?P<page_pk>\d+)/new$', documents.new, name='documents_new'),
    url(r'^documents/new$', documents.new, name='documents_new'),
    # Networks
    url(r'^networks/$', networks.index, name='networks'),
    url(r'^networks/(?P<pk>\d+)/edit$', networks.edit, name='networks_edit'),
    url(r'^networks/(?P<pk>\d+)/delete$', networks.delete, name='networks_delete'),
    url(r'^networks/new$', networks.new, name='networks_new'),
    # Slider
    url(r'^sliders/$', sliders.index, name='sliders'),
    url(r'^sliders/(?P<pk>\d+)/$', sliders.detail, name='sliders_detail'),
    url(r'^sliders/(?P<pk>\d+)/edit$', sliders.edit, name='sliders_edit'),
    url(r'^sliders/(?P<pk>\d+)/delete$', sliders.delete, name='sliders_delete'),
    url(r'^sliders/(?P<pk>\d+)/clone$', sliders.clone, name='sliders_clone'),
    url(r'^sliders/new$', sliders.new, name='sliders_new'),
    # menus
    url(r'^menus/$', menus.index, name='menus'),
    url(r'^menus/(?P<pk>\d+)/edit$', menus.edit, name='menus_edit'),
    url(r'^menus/(?P<pk>\d+)/delete$', menus.delete, name='menus_delete'),
    url(r'^menus/new$', menus.new, name='menus_new'),
    # blocks
    url(r'^blocks/$', blocks.index, name='blocks'),
    url(r'^blocks/(?P<pk>\d+)/$', blocks.detail, name='blocks_detail'),
    url(r'^blocks/(?P<pk>\d+)/edit$', blocks.edit, name='blocks_edit'),
    url(r'^blocks/(?P<pk>\d+)/delete$', blocks.delete, name='blocks_delete'),
    url(r'^blocks/new$', blocks.new, name='blocks_new'),
    # webforms
    url(r'^webforms/$', webforms.index, name='webforms'),
    url(r'^webforms/(?P<pk>\d+)/$', webforms.detail, name='webforms_detail'),
    url(r'^webforms/(?P<pk>\d+)/delete$', webforms.delete, name='webforms_delete'),
    url(r'^webforms/rooms/$', webforms.rooms, name='webforms_rooms'),
    url(r'^webforms/rooms/(?P<pk>\d+)/$', webforms.rooms_detail, name='webforms_rooms_detail'),
    url(r'^webforms/tours/$', webforms.tours, name='webforms_tours'),
    url(r'^webforms/tours/(?P<pk>\d+)/$', webforms.tours_detail, name='webforms_tours_detail'),
    # Redirects
    url(r'^redirects/$', redirects.index, name='redirects'),
    url(r'^redirects/(?P<pk>\d+)/edit$', redirects.edit, name='redirects_edit'),
    url(r'^redirects/(?P<pk>\d+)/delete$', redirects.delete, name='redirects_delete'),
    url(r'^redirects/new$', redirects.new, name='redirects_new'),
    # reports
    url(r'^reports/notfound$', reports.notfound, name='reports_notfound'),
]
