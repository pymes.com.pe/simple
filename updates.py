from business.models import Web
from cms.models import Appearance
for obj in Appearance.objects.all():
    Web.objects.filter(pk=obj.web_id).update(template_id=obj.template_id, color_id=obj.color_id, template_type=obj.template_type, logo = obj.logo, favicon=obj.favicon)