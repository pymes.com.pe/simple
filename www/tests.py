from django.contrib.auth.models import User
from django.test import TestCase
import unittest
from django.test import Client
from django.urls import reverse
from cms import models
from business.models import Web, Template, Color

class CmsTestCase(TestCase):
    pk = 1
    code = 12345678901234567890123456789012
    def setUp(self):
        # Crear usuario
        user = User.objects.create(username='juan')
        user.set_password('juan132')
        user.save()
        # Autenticar usuario
        self.client = Client()
        self.client.login(username='juan', password='juan132')
        # Template
        Template.objects.create(name="First Template")
        Color.objects.create(hexadecimal="111222", template_id=1)
        # Web
        domain_pk = 1
        Web.objects.create(code="12345678901234567890123456789012", domain="maspid.com", owner_id=1)
        models.Node.objects.create(web_id=domain_pk,title="Basic Page", slug="about-us", in_menu=True)
        models.Network.objects.create(web_id=domain_pk,link="https://facebook.com")
        models.Block.objects.create(web_id=domain_pk,title="Block 1", body="Body")
        models.Slider.objects.create(web_id=domain_pk,title="Slider 1", image="slider.jpg")

    def test_web(self):
        # New 200
        response = self.client.get(reverse("web_info",args=(self.code,self.pk)))
        self.assertEqual(response.status_code, 200)
        # New 200
        response = self.client.get(reverse("web_languages",args=(self.code,self.pk)))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse("web_sitemap_all",args=(self.code,self.pk)))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse("web_sitemap",args=(self.code,self.pk,"es")))
        self.assertEqual(response.status_code, 200)

    def test_pages(self):
        # New 200
        response = self.client.get(reverse("web_page_default",args=(self.code,self.pk)))
        self.assertEqual(response.status_code, 200)
        # New 200
        response = self.client.get(reverse("web_page_language",args=(self.code,self.pk,"es")))
        self.assertEqual(response.status_code, 200)
        # New 200
        response = self.client.get(reverse("web_page_language",args=(self.code,self.pk,"en")))
        self.assertEqual(response.status_code, 200)

    def test_fronts(self):
        # New 200
        response = self.client.get(reverse("web_home_redirect",args=(self.code,self.pk,"es")))
        self.assertEqual(response.status_code, 200)
        # New 200
        response = self.client.get(reverse("web_default_home",args=(self.code,self.pk)))
        self.assertEqual(response.status_code, 200)
        # New 200
        response = self.client.get(reverse("web_home_not_found",args=(self.code,self.pk,"es")))
        self.assertEqual(response.status_code, 200)
        # New 200
        response = self.client.get(reverse("web_home",args=(self.code,self.pk,"es")))
        self.assertEqual(response.status_code, 200)
        # New 200
        response = self.client.get(reverse("web_home",args=(self.code,self.pk,"en")))
        self.assertEqual(response.status_code, 200)

    def test_nodes(self):
        response = self.client.get(reverse("web_node",args=(self.code,self.pk,"es","about-us",)))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse("web_not_found",args=(self.code,self.pk,"not_found.html")))
        self.assertEqual(response.status_code, 200)