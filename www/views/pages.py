from django.http.response import JsonResponse, HttpResponse
from cms.models import Translation, Appearance, Block, Node, Network, Slider, Menu
from webform.models import Contact
from business.models import Web
from common.models import NotFound, Redirect
from common.faster import get_network

def web_page(web_code, web_pk, lang_code):
    try:
        web = Web.objects.values("language").get(pk=web_pk, code=web_code)
    except Web.DoesNotExist:
        ctn = {
            "status":404,
        }
        return ctn
    # Home URL
    front = "/{0}/".format(lang_code)
    if web["language"] == lang_code:
        front = "/"
    try:
        current_front = Translation.objects.get(web_id=web_pk, language=lang_code)
    except Translation.DoesNotExist:
        ctn = {
            "status":404,
        }
        return ctn

    appearance = Appearance.objects.get(pk=web_pk)
    promoted_query = Node.objects.filter(web_id=web_pk, promoted=True, language=lang_code)[:8]
    promoted = []
    for o in promoted_query:
        e = {
            "title": o.title,
            "node_type": o.node_type,
            "summary": o.summary,
            "money": o.get_money_display(),
            "price": o.get_price(),
            "duration":o.quantity,
            "quantity":o.quantity,
            "image":o.image.url,
            "image_small":o.image.thumb.url,
            "url":"/{0}/{1}".format(o.language, o.slug)
        }
        promoted.append(e)

    front_query = Node.objects.filter(web_id=web_pk, front=True, language=lang_code)[:4]
    promoted_front = []
    for o in front_query:
        e = {
            "title": o.title,
            "node_type": o.node_type,
            "summary": o.summary,
            "money": o.get_money_display(),
            "price": o.get_price(),
            "duration":o.quantity,
            "quantity":o.quantity,
            "image":o.image.url,
            "image_small":o.image.thumb.url,
            "url":"/{0}/{1}".format(o.language, o.slug)
        }
        promoted_front.append(e)
    sliders = []
    for o in Slider.objects.filter(web_id=web_pk, status=True, language=lang_code)[:4]:
        e = {
            "title": o.title,
            "description": o.body,
            "image": o.image.url,
            "image_medium": o.image.medium.url,
            "image_small": o.image.thumb.url,
            "link_title": o.link_title,
            "link_url": o.link_url,
        }
        sliders.append(e)
    menu = []
    for item in Menu.objects.filter(web_id=web_pk, parent__isnull=True, language=lang_code):
        submenu = []
        num_elements = 0
        for subitem in item.menu_set.all():
            sub = {
                "id": subitem.pk,
                "title": subitem.title,
                "description": subitem.description,
                "url": subitem.uri(),
                "parent": subitem.parent_id,
            }
            submenu.append(sub)
            num_elements = num_elements + 1
        e = {
            "id": item.pk,
            "title": item.title,
            "description": item.description,
            "expanded": item.expanded,
            "url": item.uri(),
            "num_elements":num_elements,
            "submenu": submenu,
        }
        menu.append(e)
    networks = []
    for o in Network.objects.filter(web_id=web_pk):
        e = {
            "name": get_network(o.link),
            "url": o.link,
        }
        networks.append(e)

    blocks = []
    for o in Block.objects.filter(web_id=web_pk, language=lang_code)[:6]:
        b = {
            "pk": o.pk,
            "title": o.title,
            "body": o.body,
            "css_style": o.css_style,
            "css_class": o.css_class,
            "format": o.block_type,
            "selector_right": o.selector_right,
            "selector_left": o.selector_left,
        }
        if o.image:
            b["image"] = o.image.url
        blocks.append(b)
    ctn = {
        "id":web_pk,
        "status":200,
        "front":front,
        "url":front,
        "language":lang_code,
        "copyright":appearance.copyright,
        "video":appearance.video,
        "background":appearance.background.url,
        "css":appearance.css,
        "js":appearance.js,
        "is_slider_full":appearance.is_slider_full,
        "widgets":appearance.widgets,
        "social_widgets":appearance.social_widgets,
        "book_room":current_front.book_hotel,
        "book_tour":current_front.book_tour,
        "contact_us":current_front.contact_us,
        "google_map":appearance.google_map,
        "featured":current_front.featured,
        "google_analytics":appearance.google_analytics,
        "facebook_pixel":appearance.facebook_pixel,
        "slogan": current_front.slogan,
        "address": current_front.address,
        "city": current_front.city,
        "business_hours":current_front.business_hours,
        # Slider
        "sliders":sliders,
        # NETWORKS
        "networks":networks,
        "promoted":promoted,
        "promoted_front":promoted_front,
        # MAIN MENU
        "menu":menu,
        "blocks":blocks,
    }

    return ctn

def web_page_default(request, web_code, web_pk):
    web = Web.objects.values("language").get(pk=web_pk)
    ctn = web_page(web_code, web_pk, web["language"])
    return JsonResponse(ctn)

def web_page_laguage(request, web_code, web_pk, lang_code):
    ctn = web_page(web_code, web_pk, lang_code)
    return JsonResponse(ctn)