from django.http.response import JsonResponse, HttpResponse
from cms.models import Node
from webform.models import Contact
from business.models import Web
from common.models import NotFound, Redirect
from common.faster import get_network
from common.send_mail import new_message
from django.views.decorators.csrf import csrf_exempt
from www.forms import ContactForm
import threading

def home_not_found(request, web_code, web_pk, lang_code):
    ctn = {
            "status":404,
            "new_path":"/",
        }
    return JsonResponse(ctn)

def not_found(request, web_code, web_pk, slug, lang_code=None):
    if lang_code:
        new_slug = "/{1}/{0}".format(slug,lang_code)
    else:
        new_slug = "/{0}".format(slug)
    try:# Verify if the slug exist in redirect 301
        obj = Redirect.objects.get(old_path=new_slug, web_id=web_pk)
        ctn = {
            "status":301,
            "new_path":obj.new_path,
        }
    except Redirect.DoesNotExist:
        try: # save URL in NotFound
            obj = NotFound.objects.get(path=new_slug, web_id=web_pk)
            obj.count = obj.count + 1
            obj.save()

        except NotFound.DoesNotExist:
            NotFound.objects.create(path=new_slug,web_id=web_pk)
        ctn = {
            "status":404,
            "new_path":"/",
            "price":0,
        }
    return JsonResponse(ctn)


def web_node(request, web_code, web_pk, lang_code, slug):
    """
    try:
        web = Web.objects.values("id").get(pk=web_pk,code=web_code)
    except Web.DoesNotExist:
        ctn = {
            "status":404,
        }
        return JsonResponse(ctn)
    """
    try:
        obj = Node.objects.get(slug=slug, language=lang_code, web_id=web_pk)
    except Node.DoesNotExist:
        new_slug = "/{0}/{1}".format(lang_code, slug)
        try:# Verify if the slug exist in redirect 301
            obj = Redirect.objects.get(old_path=new_slug, web_id=web_pk)
            ctn = {
                "status":301,
                "new_path":obj.new_path,
            }
            return JsonResponse(ctn)
        except Redirect.DoesNotExist:
            try:# Verify if the slug exist in redirect 301
                obj = NotFound.objects.get(path=new_slug, web_id=web_pk)
                obj.count = obj.count + 1
                obj.save()

            except NotFound.DoesNotExist:
                NotFound.objects.create(path=new_slug,web_id=web_pk)
            ctn = {
                "status":404,
                "price":0,
                "seo": {"page_title":"404 - No encontrado"},
            }
            return JsonResponse(ctn)
    contents_by_type = []
    pictures_temp = []
    if obj.node_type == "b":
        contents_by_type = Node.objects.filter(node_type="e", web_id=web_pk, language=lang_code)
    elif obj.node_type == "c":
        contents_by_type = Node.objects.filter(category_id=obj.pk, language=lang_code).order_by('weight')
    elif obj.node_type == "p":
        # Imagenes de la galeria para el slider de nodo
        pictures_temp = obj.images.all()[:4]
    elif obj.node_type == "g":
        contents_by_type = []
        pictures_temp = obj.images.all()
    elif obj.node_type == "t" or obj.node_type == "r" or obj.node_type == "s":
        contents_by_type = []
        pictures_temp = obj.images.all()[:4]
    # Sub Elements
    subs = []
    num_elements = 0
    for o in contents_by_type:
        e = {
            "title": o.title,
            "node_type": o.node_type,
            "summary": o.summary,
            "update": o.update_at,
            "money": o.get_money_display(),
            "price": o.price,
            "duration":o.quantity,
            "quantity":o.quantity,
            "image_medium": o.image.medium.url,
            "image_small": o.image.thumb.url,
            "url": "/{0}/{1}".format(o.language, o.slug),
        }
        subs.append(e)
        num_elements = num_elements + 1

    # Photo Gallery
    pictures = []
    for o in pictures_temp:
        e = {
            "small": o.image.thumb.url,
            "medium": o.image.medium.url,
            "original": o.image.url,
            "caption": o.alt,
        }
        pictures.append(e)

    ctn = {
        "id":obj.pk,
        "status":200,
        "amp":obj.amp,
        "robot_index":obj.robot_index,
        "url": "/{0}/{1}".format(obj.language, obj.slug),
        "title":obj.title,
        "summary":obj.summary,
        "language":obj.language,
        "money":obj.get_money_display(),
        "price":obj.get_price(),
        "body":obj.body,
        "include":obj.include,
        "recommendations":obj.recommendations,
        "quantity":obj.quantity,
        "duration":obj.quantity,
        "node_type":obj.node_type,
        "with_image":obj.with_image,
        "image":obj.image.url,
        "image_medium":obj.image.medium.url,
        "image_small":obj.image.thumb.url,
        "script":obj.script,
        "seo":{
            "page_title":obj.page_title,
            "keywords":obj.keywords,
            "description":obj.description,
        },
        "num_objs":num_elements, # Number sub elements
        "objs":subs, # Number sub elements
        "pictures":pictures # Pictures for gallery photos or slider
    }
    return JsonResponse(ctn)
