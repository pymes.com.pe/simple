from django.http.response import JsonResponse, HttpResponse
from cms.models import Translation, Node
from business.models import Web
from common.models import NotFound, Redirect

def domain_www(www):
    if www:
        return "www."
    return ""

def info(request, web_code, web_pk):
    try:
        obj = Web.objects.get(pk=web_pk, code=web_code)
    except Web.DoesNotExist:
        ctn = {
            "status":404,
            "pk":0,
            "code":123456,
            "domain":"notfound.com",
            "multi_language":False,
            "language":"es",
            "ssl":False
        }
        return JsonResponse(ctn)

    trans_qs = Translation.objects.filter(web_id=web_pk, status=True).values("language")
    languages = []
    for trans in trans_qs:
        languages.append(trans["language"])

    ctn = {
            "pk":web_pk,
            "code":web_code,
            "domain":obj.domain,
            "ssl":obj.protocol,
            "status":200,
            "robot_index": obj.status,
            "company_type":obj.company_type,
            "name":obj.name,
            "short_name":obj.short_name,
            "phone":obj.phone,
            "phones":obj.phones,
            "mobile":obj.mobile,
            "mobiles":obj.mobiles,
            "email":obj.email_first,
            "email_alt":obj.email_second,
            "with_www":domain_www(obj.with_www),
            "multi_language":obj.multi_language,
            "language":obj.language,
            "languages":languages,
            "culqi_key_public":obj.culqi_key_public,
            "culqi_key_private":obj.culqi_key_private,
            "culqi_commission":"",
            "visa_link":obj.visa_link,
            "visa_commission":obj.visa_commission,
            "paypal_link":obj.paypal_link,
            "paypal_commission":obj.paypal_commission,
            "template":obj.template.dir_name,
            "color":"pymesc{}".format(obj.color.hexadecimal),
            "hex":obj.color.hexadecimal,
            "file_css":obj.template.css,
            "file_js":obj.template.js,
            "crm":obj.crm,
            "cache":obj.crm,
            "payment_gateway":obj.payment_gateway,
            "logo": obj.logo.url,
            "favicon": obj.favicon.url
        }

    return JsonResponse(ctn)

def sitemap_all(request, web_code, web_pk):
    try:
        web = Web.objects.values("id","domain","protocol","language","with_www").get(id=web_pk, code=web_code)
    except Web.DoesNotExist:
        ctn = {
            "status":404,
        }
        return JsonResponse(ctn)

    obj_queryset = Node.objects.filter(web_id=web_pk).values("language","slug","node_type")

    uri = "http://{0}/".format(web["domain"])
    if web["with_www"]:
        uri = "http://www.{0}/".format(web["domain"])

    if web["protocol"]:
        uri = "https://{0}/".format(web["domain"])
        if web["with_www"]:
            uri = "https://www.{0}/".format(web["domain"])

    e = {
        "url":uri,
        "priority":1,
    }
    obj_list = [e]
    for o in obj_queryset:
        e = {
            "url": "{0}{1}/{2}".format(uri,o["language"],o["slug"]),
            "priority": 0.8,
        }
        if o["node_type"] != "c":
            e["priority"] = 0.5

        obj_list.append(e)
    ctn = {
        "status":201,
        "title": "Site Map",
        "objs":obj_list
    }
    return JsonResponse(ctn)

def sitemap(request, web_code, web_pk, lang_code):
    try:
        web = Web.objects.values("id","domain","protocol","language","with_www").get(id=web_pk, code=web_code)
    except Web.DoesNotExist:
        ctn = {
            "status":404,
        }
        return JsonResponse(ctn)
    try:
        front = Translation.objects.values("id").get(web_id=web_pk,language=lang_code)
    except Translation.DoesNotExist:
        ctn = {
            "status":301,
            "new_path":"/{0}/sitemap.xml".format(web["language"]),
        }
        return JsonResponse(ctn)
    obj_queryset = Node.objects.filter(web_id=web_pk, language=lang_code).values("language","slug","node_type")

    uri = "http://{0}/".format(web["domain"])
    if web["with_www"]:
        uri = "http://www.{0}/".format(web["domain"])

    if web["protocol"]:
        uri = "https://{0}/".format(web["domain"])
        if web["with_www"]:
            uri = "https://www.{0}/".format(web["domain"])
    e = {
        "url":uri,
        "priority":1,
    }
    obj_list = [e]
    for o in obj_queryset:
        e = {
            "url": "{0}{1}/{2}".format(uri,o["language"],o["slug"]),
            "priority": 0.8,
        }
        if o["node_type"] != "c":
            e["priority"] = 0.5

        obj_list.append(e)
    ctn = {
        "status":201,
        "title": "Site Map",
        "objs":obj_list
    }
    return JsonResponse(ctn)

def languages(request, web_code, web_pk):
    try:
        obj = Web.objects.values("domain").get(pk=web_pk, code=web_code)
    except Web.DoesNotExist:
        ctn = {}
        return JsonResponse(ctn)
    objs = Translation.objects.filter(web_id=web_pk).values("language")
    languages = []
    for trans in objs:
        languages.append(trans["language"])
    return JsonResponse({'languages':languages})