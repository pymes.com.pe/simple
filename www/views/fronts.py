from django.http.response import JsonResponse, HttpResponse
from cms.models import Translation
from business.models import Web


def web_home_redirect(request, web_code, web_pk, lang_code):
    query_set = Translation.objects.filter(web_id=web_pk,language=lang_code).values("id")
    if query_set.exists():
        ctn = {
            "status":301,
            "new_path":"/{0}/".format(lang_code),
        }
    else:
        ctn = {
            "status":301,
            "new_path":"/",
        }
    return JsonResponse(ctn)

def web_front(web_code, web_pk, lang_code):
    try:
        web = Web.objects.values("language").get(pk=web_pk, code=web_code)
    except Web.DoesNotExist:
        ctn = {
            "status":404,
        }
        return ctn
    # Home URL
    url = "/{0}/".format(lang_code)
    if web["language"] == lang_code:
        url = "/"

    try:
        front = Translation.objects.get(web_id=web_pk,language=lang_code)
    except Translation.DoesNotExist:
        ctn = {
            "status":301,
            "new_path":"/",
        }
        return ctn
    ctn = {
        "status":200,
        "id":1,
        "robot_index":True,
        "url":url,
        "title": front.title,
        "body": front.body,
        "link_title": front.link_title,
        "link_url": front.link_url,
        "node_type":"f",
        "price":0,
        "seo":{
            "page_title":front.page_title,
            "keywords":front.keywords,
            "description":front.description,
        },
        "image": front.image.url,
        "image_small": front.image.url,
        "image_medium": front.image.url,
    }
    return ctn

def web_default_language(request, web_code, web_pk):
    web = Web.objects.values("language").get(pk=web_pk)
    ctn = web_front(web_code,web_pk, web["language"])
    return JsonResponse(ctn)

def web_home(request, web_code, web_pk, lang_code):
    web = Web.objects.values("language").get(pk=web_pk)
    if lang_code == web["language"]:
        ctn = {
            "status":301,
            "new_path":"/",
        }
        return JsonResponse(ctn)
    ctn = web_front(web_code,web_pk, lang_code)
    return JsonResponse(ctn)

