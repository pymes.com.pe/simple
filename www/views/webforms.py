from django.http.response import JsonResponse, HttpResponse
from webform.models import Contact, BookTour, BookRoom, Suscription
from business.models import Web
from common.send_mail import new_message, new_book_room, new_book_tour
from django.views.decorators.csrf import csrf_exempt
from www.forms import ContactForm, BookTourForm, BookRoomForm, SuscriptionForm
import threading

@csrf_exempt
def contact_us(request, web_code, web_pk):
    try:
        web = Web.objects.values("id","email_first","email_second","name").get(pk=web_pk)
    except Web.DoesNotExist:
        ctn = {
            "status":404,
        }
        return JsonResponse(ctn)
    ctn = {}
    if request.method == "POST":
        obj = Contact(web_id=web_pk)
        form = ContactForm(request.POST, instance=obj)
        if form.is_valid():
            message = form.save()
            # Trhead for send mail
            h = threading.Thread(target=new_message, args=(message,web,))
            h.start()
            ctn["status"] = 200
        else:
            ctn["status"] = 201
    else:
        ctn["status"] = 300
    return JsonResponse(ctn)

@csrf_exempt
def book_tour(request, web_code, web_pk):
    try:
        web = Web.objects.values("id","email_first","email_second","name").get(pk=web_pk)
    except Web.DoesNotExist:
        ctn = {
            "status":404,
        }
        return JsonResponse(ctn)

    ctn = {}
    if request.method == "POST":
        obj = BookTour(web_id=web_pk)
        form = BookTourForm(request.POST, instance=obj)
        if form.is_valid():
            message = form.save()
            # Trhead for send mail
            h = threading.Thread(target=new_book_tour, args=(message,web,))
            h.start()
            ctn["status"] = 200
        else:
            ctn["status"] = 201
    else:
        ctn["status"] = 300
    return JsonResponse(ctn)

@csrf_exempt
def book_room(request, web_code, web_pk):
    try:
        web = Web.objects.values("id","email_first","email_second","name").get(pk=web_pk)
    except Web.DoesNotExist:
        ctn = {
            "status":404,
        }
        return JsonResponse(ctn)
    ctn = {}
    if request.method == "POST":
        obj = BookRoom(web_id=web_pk)
        form = BookRoomForm(request.POST, instance=obj)
        if form.is_valid():
            message = form.save()
            # Trhead for send mail
            h = threading.Thread(target=new_book_room, args=(message,web,))
            h.start()
            ctn["status"] = 200
        else:
            ctn["status"] = 201
    else:
        ctn["status"] = 300

    return JsonResponse(ctn)

@csrf_exempt
def suscription(request, web_code, web_pk):
    try:
        web = Web.objects.values("id","email_first","email_second","name").get(pk=web_pk)
    except Web.DoesNotExist:
        ctn = {
            "status":404,
        }
        return JsonResponse(ctn)
    ctn = {}
    if request.method == "POST":
        obj = Suscription(web_id=web_pk)
        form = SuscriptionForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            # Trhead for send mail
            #h = threading.Thread(target=new_message, args=(message,web,))
            #h.start()
            ctn["status"] = 200
        else:
            ctn["status"] = 201
    else:
        ctn["status"] = 300
    return JsonResponse(ctn)
