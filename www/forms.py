# -*- coding: utf-8 -*-
from django.forms import ModelForm
from webform.models import Contact, BookTour, BookRoom, Suscription


class ContactForm(ModelForm):
    class Meta:
        model = Contact
        exclude = ('estado',)


class BookTourForm(ModelForm):
    class Meta:
        model = BookTour
        exclude = ('estado','country')


class BookRoomForm(ModelForm):
    class Meta:
        model = BookRoom
        exclude = ('estado',)


class SuscriptionForm(ModelForm):
    class Meta:
        model = Suscription
        exclude = ('estado',)
