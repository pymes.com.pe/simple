# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.decorators import permission_required, login_required
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from business.models import Web
from business import forms
from cms.models import Appearance, Translation


def user(request):
    if request.user.is_authenticated:
        if request.user.is_superuser:
            search = request.GET.get('search', None)
            fronts = None
            if search:
                objs_list = Web.objects.filter(domain__icontains=search)
            else:
                search = ""
                objs_list = Web.objects.all()

            paginator = Paginator(objs_list, 10)

            page = request.GET.get('page')
            try:
                objs = paginator.page(page)
            except PageNotAnInteger:
                objs = paginator.page(1)
            except EmptyPage:
                objs = paginator.page(paginator.num_pages)
            ctx = {
                "objs":objs,
            }
            template = loader.get_template('domains/index.html')
            return HttpResponse(template.render(ctx, request))
        else:
            user_id = request.user.id
            webs_count = Web.objects.filter(owner_id=user_id).count()
            if webs_count == 0 :
                ctx = {
                    "objs":None,
                }
                template = loader.get_template('dashboard.html')
                return HttpResponse(template.render(ctx, request))
            if webs_count == 1 :
                web = Web.objects.get(owner_id=user_id)
                appearance = Appearance.objects.get(pk=web.pk)
                translations = Translation.objects.filter(web_id=web.pk).values('title','language','id')
                ctx = {
                    "web_pk":web.pk,
                    "web_domain":web.domain,
                    "web_protocol":web.protocol,
                    "appearance":appearance,
                    "translations":translations,
                    "obj":web,
                }
                template = loader.get_template('domains/dashboard.html')
                return HttpResponse(template.render(ctx, request))
            else:
                objs_list = Web.objects.filter(owner_id=user_id)
                paginator = Paginator(objs_list, 6)

                page = request.GET.get('page')
                try:
                    objs = paginator.page(page)
                except PageNotAnInteger:
                    objs = paginator.page(1)
                except EmptyPage:
                    objs = paginator.page(paginator.num_pages)

                ctx = {
                    "objs":objs,
                }
                template = loader.get_template('domains/index.html')
                return HttpResponse(template.render(ctx, request))
    else:
        ctx = {}
        template = loader.get_template('public.html')
        return HttpResponse(template.render(ctx, request))

@login_required()
def new(request):
    owner_id = request.user.id
    obj = Web(owner_id=owner_id)
    if request.method == "POST":
        if request.user.is_superuser:
            form = forms.NewForm(request.POST, instance=obj)
            if form.is_valid():
                new = form.save()
                messages.success(request, "Dominio creado")
                return HttpResponseRedirect(reverse("webs_dashboard",args=(new.pk,)))
            else:
                messages.warning(request,"Verifique los campos obligatorios")
        else:
            form = forms.TicketForm(request.POST, instance=obj)
            if form.is_valid():
                form.save()
                messages.success(request, "Solicitud de dominio enviado")
                return HttpResponseRedirect(reverse("dashboard"))
            else:
                messages.warning(request,"Verifique los campos obligatorios")
    else:
        form = forms.NewForm(instance=obj)
    form_ticket = forms.TicketForm()
    ctx = {
        "form":form,
        "form_ticket":form_ticket
    }
    template = loader.get_template('domains/new.html')
    return HttpResponse(template.render(ctx, request))

@login_required()
def info(request,web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.warning(request, "Dominio no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))
    if request.method == 'POST':
        form = forms.InfoForm(request.POST, request.FILES, instance=web)
        if form.is_valid():
            form.save()
            messages.success(request, "Información actualizado.")
            return HttpResponseRedirect(reverse("webs_dashboard", args=(web_pk,)))
        else:
            messages.warning(request, 'Verifique la información completada.')
    else:
        form = forms.InfoForm(instance=web)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web.domain,
        'form': form,
        }

    template = loader.get_template('domains/info.html')
    return HttpResponse(template.render(ctx, request))

@login_required()
def contact(request,web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.warning(request, "Dominio no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))
    if request.method == 'POST':
        form = forms.ContactForm(request.POST, instance=web)
        if form.is_valid():
            form.save()
            messages.success(request, "Información actualizado.")
            return HttpResponseRedirect(reverse("webs_dashboard", args=(web_pk,)))
        else:
            messages.warning(request, 'Verifique la información completada.')
    else:
        form = forms.ContactForm(instance=web)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web.domain,
        'form': form,
        }
    template = loader.get_template('domains/contact.html')
    return HttpResponse(template.render(ctx, request))

@login_required()
def domain_change(request,web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.warning(request, "Dominio no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    if request.method == 'POST':
        form = forms.NameForm(request.POST, instance=web)
        if form.is_valid():
            form.save()
            messages.success(request, "Dominio actualizado.")
            return HttpResponseRedirect(reverse("webs_dashboard", args=(web_pk,)))
        else:
            messages.warning(request, 'Verifique la información completada.')
    else:
        form = forms.NameForm(instance=web)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web.domain,
        'form': form,
        }
    template = loader.get_template('domains/change_domain.html')
    return HttpResponse(template.render(ctx, request))


@permission_required("is_superuser")
def change_owner(request,web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.warning(request, "Dominio no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))
    if request.method == 'POST':
        form = forms.ChangeOwnerForm(request.POST, instance=web)
        if form.is_valid():
            form.save()
            messages.success(request, "Dominio transferido.")
            return HttpResponseRedirect(reverse("webs_dashboard", args=(web_pk,)))
        else:
            messages.warning(request, 'Verifique la información completada.')
    else:
        form = forms.ChangeOwnerForm(instance=web)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web.domain,
        'form': form,
        }
    template = loader.get_template('domains/change_owner.html')
    return HttpResponse(template.render(ctx, request))


@login_required()
def design(request,web_pk):
    try:
        if request.user.is_superuser:
            obj = Web.objects.get(pk=web_pk)
        else:
            owner_id = request.user.id
            obj = Web.objects.get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.info(request, "Web no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))

    if request.method == 'POST':
        form = forms.DesignForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, "Template y color actualizado.")
            return HttpResponseRedirect(reverse("clear_cache", args=(web_pk,)))
        else:
            messages.warning(request, 'Falta elegir el color.')
    else:
        form = forms.DesignForm(instance=obj)
    ctx = {
        "web_pk":web_pk,
        "web_domain":obj.domain,
        "web_protocol":obj.protocol,
        'form': form,
        "color":obj.color_id,
        }
    template = loader.get_template('domains/template.html')
    return HttpResponse(template.render(ctx, request))


@permission_required("is_superuser")
def server(request,web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.warning(request, "Dominio no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))
    if request.method == 'POST':
        form = ServerForm(request.POST, instance=web)
        if form.is_valid():
            form.save()
            messages.success(request, "Servidores actualizados.")
            return HttpResponseRedirect(reverse("webs_dashboard", args=(web_pk,)))
        else:
            messages.warning(request, 'Verifique la información completada.')
    else:
        form = ServerForm(instance=web)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web.domain,
        'form': form,
        }
    template = loader.get_template('domains/change_owner.html')
    return HttpResponse(template.render(ctx, request))

@permission_required("is_superuser")
def payment(request,web_pk):
    try:
        if request.user.is_superuser:
            web = Web.objects.get(pk=web_pk)
        else:
            owner_id = request.user.id
            web = Web.objects.get(pk=web_pk, owner_id=owner_id)
    except Web.DoesNotExist:
        messages.warning(request, "Dominio no encontrado.")
        return HttpResponseRedirect(reverse("dashboard"))
    if request.method == 'POST':
        form = forms.PaymentForm(request.POST, instance=web)
        if form.is_valid():
            form.save()
            messages.success(request, "Metodos de pago actualizado.")
            return HttpResponseRedirect(reverse("webs_dashboard", args=(web_pk,)))
        else:
            messages.warning(request, 'Verifique la información completada.')
    else:
        form = forms.PaymentForm(instance=web)
    ctx = {
        "web_pk":web_pk,
        "web_domain":web.domain,
        'form': form,
        }
    template = loader.get_template('domains/change_owner.html')
    return HttpResponse(template.render(ctx, request))
