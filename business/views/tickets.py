# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from business.models import Ticket
from business.forms import TicketForm

@login_required
def index(request):
    if request.user.is_superuser:
        objs_list = Ticket.objects.all()
    else:
        objs_list = Ticket.objects.filter(owner_id=request.user.id)
    paginator = Paginator(objs_list, 25)

    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)
    ctx = {
        "objs":objs,
    }
    return render(request, "tickets/index.html", ctx)

@login_required
def detail(request,pk):
    try:
        if request.user.is_superuser:
            obj = Ticket.objects.get(pk=pk)
        else:
            obj = Ticket.objects.get(pk=pk, owner_id=request.user.id)
    except Ticket.DoesNotExist:
        messages.warning(request,"Ticket no encontrada.")
        return HttpResponseRedirect(reverse("tickets"))
    if obj.is_new and request.user.is_superuser:
        obj.is_new = False
        obj.save()
    ctx = {
        "obj":obj,
    }
    return render(request,"tickets/detail.html", ctx)


@login_required
def new(request):
    owner_id = request.user.id
    if request.method == "POST":
        obj = Ticket(owner_id=owner_id)
        form = TicketForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            # Notificar de nuevo mensaje
            messages.info(request, 'Ticket enviado')
            return HttpResponseRedirect(reverse("tickets"))
        else:
            messages.warning(request,"Revise los campos.")
    else:
        form = TicketForm()
    ctx = {
        "form":form,
    }
    return render(request,"tickets/new.html", ctx)