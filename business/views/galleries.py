# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from business.models import Gallery
from business.forms import GalleryForm

@login_required()
def index(request):
    search = request.GET.get('search', None)
    if search:
        objs_list = Gallery.objects.filter(title__icontains=search)
    else:
        search = ""
        objs_list = Gallery.objects.all()

    paginator = Paginator(objs_list, 8)
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)
    ctx = {
        "objs":objs,
        "search":search,
    }
    return render(request, "business/gallery_list.html", ctx)

@login_required()
def update(request,pk):
    try:
        obj = Gallery.objects.get(pk=pk)
    except Gallery.DoesNotExist:
        messages.warning(request,"Gallery no encontrada.")
        return HttpResponseRedirect(reverse("galleries"))
    if request.method == "POST":
        form = GalleryForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            form.save()
            messages.info(request, 'Imagen actualizada')
            return HttpResponseRedirect(reverse("galleries"))
        else:
            messages.warning(request,"Revise los campos.")

    else:
        form = GalleryForm(instance=obj)
    ctx = {
        "form":form,
    }
    return render(request,"business/gallery_form.html", ctx)


@login_required()
def new(request):
    if request.method == "POST":
        obj = Gallery()
        form = GalleryForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            form.save()
            # Notificar de nuevo mensaje
            messages.info(request, 'Imagen agregado')
            return HttpResponseRedirect(reverse("galleries_new"))
        else:
            messages.warning(request,"Revise los campos.")
    else:
        form = GalleryForm()
    ctx = {
        "form":form,
    }
    return render(request,"business/gallery_form.html", ctx)