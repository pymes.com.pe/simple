# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth.decorators import permission_required, login_required
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from business.models import Template, Color
from business.forms import TemplateForm, ColorForm, ImportForm, TemplateEditForm

@login_required()
def index(request):
    objs_list = Template.objects.all()
    paginator = Paginator(objs_list, 10)

    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)
    ctx = {
        "objs":objs,
    }
    return render(request, "templates/index.html", ctx)

@login_required()
def detail(request,pk):
    try:
        obj = Template.objects.get(pk=pk)
    except Template.DoesNotExist:
        messages.warning(request,"Template no encontrado.")
        return HttpResponseRedirect(reverse("templates"))
    ctx = {
        "obj":obj,
    }
    return render(request,"templates/detail.html", ctx)

@permission_required("is_superuser")
def edit(request,pk):
    try:
        obj = Template.objects.get(pk=pk)
    except Template.DoesNotExist:
        messages.warning(request,"Template no encontrado.")
        return HttpResponseRedirect(reverse("templates"))

    if request.method == "POST":
        form = TemplateEditForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request,"Template actualizado.")
            return HttpResponseRedirect(reverse("templates_detail",args=(pk,)))
        else:
            messages.warning(request,"Revise los campos.")
    else:
        form = TemplateEditForm(instance=obj)
    ctx = {
        "form":form,
    }
    return render(request,"templates/edit.html", ctx)

@permission_required("is_superuser")
def new(request):
    if request.method == "POST":
        obj = Template()
        form = TemplateForm(request.POST, instance=obj)
        if form.is_valid():
            new_obj = form.save()
            messages.success(request, "Template registrado.")
            return HttpResponseRedirect(reverse("templates_detail",args=(new_obj.id,)))
        else:
            messages.warning(request, 'Revise los campos:')
    else:
        form = TemplateForm()
    ctx = {
        "form":form,
    }
    return render(request, "templates/new.html", ctx)

def parser_config(data_original):
    clear_data = data_original.replace("\r", "")
    clear_data = clear_data.replace("\n\n", "\n")
    rows = clear_data.split("\n")
    template = {}
    for row in rows:
        data = row.split(":",1)
        if data[1] == '-':
            template[str(data[0])] = None
        else:
            template[str(data[0])] = str(data[1])
    return template

@permission_required("is_superuser")
def import_config(request):
    if request.method == "POST":
        form = ImportForm(request.POST)
        if form.is_valid():
            data = request.POST.get("data")
            template = parser_config(data)
            if not template["pk"]:
                messages.success(request, "Template no encontado.")
                return HttpResponseRedirect(reverse("templates"))
            theme = Template.objects.get(pk=template["pk"])
            theme.name = template["name"]
            theme.dir_name = template["dir_name"]
            theme.price = template["price"]
            theme.private = template["private"]
            if template['category']:
                theme.category = template["category"]
            theme.css = template["css"]
            if template["js"]:
                theme.js = template["js"]
            theme.prefix = template["prefix"]
            theme.status = template["status"]
            theme.save()

            messages.success(request, "Template actualizado.")
            return HttpResponseRedirect(reverse("templates_detail",args=(template["pk"],)))
        else:
            messages.warning(request, 'Revise los campos:')
    else:
        form = ImportForm()
    ctx = {
        "form":form,
    }
    return render(request, "templates/import.html", ctx)

@permission_required("is_superuser")
def color(request,pk):
    try:
        obj = Template.objects.get(pk=pk)
    except Template.DoesNotExist:
        messages.warning(request,"Template no encontrado.")
        return HttpResponseRedirect(reverse("templates"))
    if request.method == "POST":
        obj = Color(template_id=pk)
        form = ColorForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request,"Color agregado.")
            return HttpResponseRedirect(reverse("templates_detail",args=(pk,)))
        else:
            messages.warning(request,"Revise los campos.")
    else:
        form = ColorForm()
    ctx = {
        "form":form,
    }
    return render(request,"templates/color.html", ctx)

@permission_required("is_superuser")
def color_edit(request,pk):
    try:
        obj = Color.objects.get(pk=pk)
    except Color.DoesNotExist:
        messages.warning(request,"Color no encontrado.")
        return HttpResponseRedirect(reverse("templates"))
    if request.method == "POST":
        form = ColorForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request,"Color actualizado.")
            return HttpResponseRedirect(reverse("templates_detail",args=(obj.template_id,)))
        else:
            messages.warning(request,"Revise los campos.")
    else:
        form = ColorForm(instance=obj)
    ctx = {
        "form":form,
    }
    return render(request,"templates/color.html", ctx)

def parser_colors(data_original):
    clear_data = data_original.replace("\r", "")
    clear_data = clear_data.replace("\n\n", "\n")
    rows = clear_data.split("\n")
    colors = []
    for row in rows:
        data = row.split("#",2)
        color = {}
        if len(data) == 3:
            if str(data[1]) == '0':
                color["pk"] = None
            else:
                color["pk"] = str(data[1])
            color["hexadecimal"] = str(data[2])
            colors.append(color)

    return colors

@permission_required("is_superuser")
def import_colors(request, template_pk):
    if request.method == "POST":
        form = ImportForm(request.POST)
        if form.is_valid():
            data = request.POST.get("data")
            colors = parser_colors(data)
            for row in colors:
                Color.objects.get_or_create(template_id = template_pk, hexadecimal = row["hexadecimal"])
            messages.success(request, "Colores importados.")
            return HttpResponseRedirect(reverse("templates_detail",args=(template_pk,)))
        else:
            messages.warning(request, 'Revise los campos:')
    else:
        form = ImportForm()
    ctx = {
        "form":form,
    }
    return render(request, "templates/import.html", ctx)