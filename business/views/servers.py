# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth.decorators import permission_required
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from business.models import Server
from business.forms import ServerForm

@permission_required("is_superuser")
def index(request):
    objs_list = Server.objects.all()

    paginator = Paginator(objs_list, 6)
    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)
    ctx = {
        "objs":objs,
    }
    return render(request, "business/server_list.html", ctx)


@permission_required("is_superuser")
def update(request,pk):
    try:
        obj = Server.objects.get(pk=pk)
    except Server.DoesNotExist:
        messages.warning(request,"Servidor no encontrada.")
        return HttpResponseRedirect(reverse("servers"))

    if request.method == "POST":
        form = ServerForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.info(request, 'Server actualizado')
            return HttpResponseRedirect(reverse("servers"))
        else:
            messages.warning(request,"Revise los campos.")

    else:
        form = ServerForm(instance=obj)
    ctx = {
        "form":form,
    }
    return render(request,"business/server_form.html", ctx)


@permission_required("is_superuser")
def new(request):
    if request.method == "POST":
        obj = Server()
        form = ServerForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.info(request, 'Server agregado')
            return HttpResponseRedirect(reverse("servers"))
        else:
            messages.warning(request,"Revise los campos.")
    else:
        form = ServerForm()
    ctx = {
        "form":form,
    }
    return render(request,"business/server_form.html", ctx)