# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth.decorators import permission_required
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django.contrib.auth.models import User
from account.forms import UserForm, UserEditForm, UserPasswordForm

@permission_required("is_superuser")
def index(request):
    objs_list = User.objects.all().order_by('id')
    paginator = Paginator(objs_list, 25)

    page = request.GET.get('page')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)
    ctx = {
        "objs":objs,
    }
    return render(request, "users/index.html", ctx)

@permission_required("is_superuser")
def detail(request,pk):
    try:
        obj = User.objects.get(pk=pk)
    except User.DoesNotExist:
        messages.warning(request,"Usuario no encontrada.")
        return HttpResponseRedirect(reverse("users"))

    ctx = {
        "obj":obj
    }
    return render(request,"users/detail.html", ctx)

@permission_required("is_superuser")
def edit(request,pk):
    try:
        obj = User.objects.get(pk=pk)
    except User.DoesNotExist:
        messages.warning(request,"Usuario no encontrada.")
        return HttpResponseRedirect(reverse("users"))

    if request.method == "POST":
        form = UserEditForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request,"Usuario actualizado.")
            return HttpResponseRedirect(reverse("users_detail",args=(pk,)))
        else:
            messages.warning(request,"Revise los campos.")
    else:
        form = UserEditForm(instance=obj)
    ctx = {
        "form":form,
    }
    return render(request,"users/edit.html", ctx)

@permission_required("is_superuser")
def password(request,pk):
    try:
        obj = User.objects.get(pk=pk)
    except User.DoesNotExist:
        messages.warning(request,"Usuario no encontrada.")
        return HttpResponseRedirect(reverse("users"))

    if request.method == "POST":
        form = UserPasswordForm(request.POST)
        if form.is_valid():
            password = request.POST.get("password")
            obj.set_password(password)
            obj.save()
            messages.success(request,"Contraseña actualizada.")
            return HttpResponseRedirect(reverse("users_detail",args=(pk,)))
        else:
            messages.warning(request,"Revise los campos.")
    else:
        form = UserPasswordForm()
    ctx = {
        "form":form,
    }
    return render(request,"users/password.html", ctx)

@permission_required("is_superuser")
def new(request):
    if request.method == "POST":
        obj = User()
        form = UserForm(request.POST, instance=obj)
        if form.is_valid():
            username = form.cleaned_data["username"]
            username = username.lower()
            password = form.cleaned_data["password"]
            email = form.cleaned_data["email"]
            user = User.objects.create_user(username, email, password)

            messages.info(request, 'Usuario registrado')
            return HttpResponseRedirect(reverse("users"))
        else:
            messages.warning(request,"Revise los campos.")
    else:
        form = UserForm()
    ctx = {
        "form":form,
    }
    return render(request,"users/new.html", ctx)