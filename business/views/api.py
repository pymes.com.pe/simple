# -*- coding: utf-8 -*-
from django.http import JsonResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from business.models import Color, Gallery, Web
from cms.models import Node

@login_required
def colors(request, template_pk):
    objs = Color.objects.filter(template_id = template_pk).values("pk","hexadecimal")

    return JsonResponse(list(objs), safe=False)

@login_required
def set_gallery(request):
    node_id = request.GET.get('node_id',None)
    gallery_id = request.GET.get('gallery_id',None)
    ctn = {
        "msg": "Error"
    }
    if gallery_id and node_id :
        try:
            node = Node.objects.get(pk=node_id)
        except Node.DoesNotExist:
            return JsonResponse(ctn, safe=False)

        if Web.objects.filter(pk=node.web_id, owner_id=request.user.id).exists() or request.user.is_superuser:
            try:
                gallery = Gallery.objects.values('image').get(pk=gallery_id)
            except Node.DoesNotExist:
                return JsonResponse(ctn, safe=False)

            node.image = gallery['image']
            node.save()
            ctn = {
                "msg": "Imagen agregado"
            }
    return JsonResponse(ctn, safe=False)