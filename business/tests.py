from django.contrib.auth.models import User
from django.test import TestCase
import unittest
from django.test import Client
from django.urls import reverse
from business import models

class BusinessTestCase(TestCase):
    def setUp(self):
        # Crear usuario
        user = User.objects.create(username='juan')
        user.set_password('juan132')
        user.is_superuser = True
        user.save()
        # Autenticar usuario
        self.client = Client()
        self.client.login(username='juan', password='juan132')
        # Template
        models.Template.objects.create(name="First Template", dir_name="basic")
        models.Color.objects.create(hexadecimal="112233", template_id=1)
        # Web
        models.Web.objects.create(domain="pymes.com.pe", owner_id=1)
        # Ticket
        models.Ticket.objects.create(domain="pymes.com.pe", subject="New Domain", message="create a domain", owner_id=1)

    def test_users(self):
        # List 200
        response = self.client.get(reverse("users"))
        self.assertEqual(response.status_code, 200)
        # Detail 200
        response = self.client.get(reverse("users_detail", args=(1,)))
        self.assertEqual(response.status_code, 200)
        # Detail 302
        response = self.client.get(reverse("users_detail", args=(4,)))
        self.assertEqual(response.status_code, 302)
        # Edit 200
        response = self.client.get(reverse("users_edit", args=(1,)))
        self.assertEqual(response.status_code, 200)
        # Edit 302
        response = self.client.get(reverse("users_edit", args=(4,)))
        self.assertEqual(response.status_code, 302)
        # New 200
        response = self.client.get(reverse("users_new"))
        self.assertEqual(response.status_code, 200)
        # Change Password 200
        response = self.client.get(reverse("users_password", args=(1,)))
        self.assertEqual(response.status_code, 200)
        # Change Password 302
        response = self.client.get(reverse("users_password", args=(2,)))
        self.assertEqual(response.status_code, 302)

    def test_webs(self):
        # List 200
        response = self.client.get(reverse("webs_new"))
        self.assertEqual(response.status_code, 200)
        # Change Owner
        response = self.client.get(reverse("change_owner", args=(1,)))
        self.assertEqual(response.status_code, 200)
        # Change Owner
        response = self.client.get(reverse("change_owner", args=(2,)))
        self.assertEqual(response.status_code, 302)
        # Contact Info 200
        response = self.client.get(reverse("domains_contact", args=(1,)))
        self.assertEqual(response.status_code, 200)
        # Contact Info 302
        response = self.client.get(reverse("domains_contact", args=(2,)))
        self.assertEqual(response.status_code, 302)
        # Business 200
        response = self.client.get(reverse("domains_info", args=(1,)))
        self.assertEqual(response.status_code, 200)
        # Business 302
        response = self.client.get(reverse("domains_info", args=(2,)))
        self.assertEqual(response.status_code, 302)
        # Change Domain 200
        response = self.client.get(reverse("domains_change", args=(1,)))
        self.assertEqual(response.status_code, 200)
        # Appearence 200
        response = self.client.get(reverse("domains_design", args=(1,)))
        self.assertEqual(response.status_code, 200)
        # Appearence 200
        response = self.client.get(reverse("domains_design", args=(2,)))
        self.assertEqual(response.status_code, 302)

    def test_templates(self):
        # List 200
        response = self.client.get(reverse("templates"))
        self.assertEqual(response.status_code, 200)
        # Detail 200
        response = self.client.get(reverse("templates_detail", args=(1,)))
        self.assertEqual(response.status_code, 200)
        # Detail 302
        response = self.client.get(reverse("templates_detail", args=(4,)))
        self.assertEqual(response.status_code, 302)
        # Edit 200
        response = self.client.get(reverse("templates_edit", args=(1,)))
        self.assertEqual(response.status_code, 200)
        # Edit 302
        response = self.client.get(reverse("templates_edit", args=(2,)))
        self.assertEqual(response.status_code, 302)
        # Color 200
        response = self.client.get(reverse("templates_color", args=(1,)))
        self.assertEqual(response.status_code, 200)
        # Color 302
        response = self.client.get(reverse("templates_color", args=(5,)))
        self.assertEqual(response.status_code, 302)
        # Edit Color 200
        response = self.client.get(reverse("templates_color_edit", args=(1,)))
        self.assertEqual(response.status_code, 200)
        # Edit Color 302
        response = self.client.get(reverse("templates_color_edit", args=(2,)))
        self.assertEqual(response.status_code, 302)
        # New 200
        response = self.client.get(reverse("templates_new"))
        self.assertEqual(response.status_code, 200)

    def test_api(self):
        # List 200
        response = self.client.get(reverse("api_colors", args=(1,)))
        self.assertEqual(response.status_code, 200)

    def test_tickets(self):
        # List 200
        response = self.client.get(reverse("tickets"))
        self.assertEqual(response.status_code, 200)
        # Detail 200
        response = self.client.get(reverse("tickets_detail", args=(1,)))
        self.assertEqual(response.status_code, 200)
        # Detail 302
        response = self.client.get(reverse("tickets_detail", args=(4,)))
        self.assertEqual(response.status_code, 302)
        # List 200
        response = self.client.get(reverse("tickets_new"))
        self.assertEqual(response.status_code, 200)