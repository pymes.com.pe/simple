# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.template.defaultfilters  import slugify
from stdimage.models import StdImageField
from uuid import uuid1
from django.db import models
from re import sub
from common import models as choices_list


SERVER_TYPE = (
    (True,'Servidor de correo'),
    (False,'Servidor de render'),
    )

class Server(models.Model):
    name = models.CharField(max_length=64, verbose_name=u'Nombre')
    server_type = models.BooleanField(default=True, verbose_name=u'Tipo de servidor', choices=SERVER_TYPE)
    url = models.URLField(max_length=64, verbose_name=u'URL')
    location = models.CharField(max_length=64, verbose_name=u'Ubicación')
    status = models.BooleanField(default=True,verbose_name=u'Estado')

    def __str__(self):
        return self.name

    def api(self):
        if self.server_type == 'e':
            return "{}api".format(self.url)
        else:
            return self.url


def web_media_dir(instance, filename):
    new_filename = sub('[^-a-zA-Z0-9_.() ]+', '', filename)
    return u'ws_{0}/media/{1}'.format(instance.id, filename)

class Template(models.Model):
    name = models.CharField(max_length=32)
    dir_name = models.CharField(max_length=32, unique=True)
    price = models.FloatField(default=0)
    private = models.BooleanField(default=False)
    screenshot = StdImageField(
        upload_to="templates",
        blank=True,
        variations={'thumb': (100, 100, True),},
        default = "node.png",
        verbose_name = u"Imagen"
        )
    css = models.CharField(max_length=32, default="pymes/")
    category = models.CharField(max_length=1, default="g", choices=choices_list.TEMPLATE_CATEGORY)
    js = models.CharField(max_length=32, blank=True)

    class Meta:
        ordering = ["id"]

    def save(self, *args, **kwargs):
        if not self.dir_name:
            self.dir_name = slugify(self.name)
        super(Template,self).save(*args,**kwargs)

    def __str__(self):
        return self.name


class Color(models.Model):
    hexadecimal = models.CharField(max_length=6)
    template = models.ForeignKey(
        Template,
        editable=False,
        on_delete=models.CASCADE
        )

    def __str__(self):
        return self.hexadecimal


class Web(models.Model):
    # Company
    code = models.CharField(max_length=32, editable=False)
    protocol =  models.BooleanField(choices=choices_list.PROTOCOL, default=False, verbose_name=u"Protocolo")
    domain = models.CharField(max_length=64, unique=True, verbose_name=u"Dominio")
    multi_language = models.BooleanField(default=False, verbose_name=u"Multi idioma")
    language = models.CharField(
        max_length=5,
        default="es",
        choices=choices_list.LANGUAGES,
        verbose_name=u"Idioma"
        )
    name = models.CharField(max_length=64, blank=True, verbose_name=u"Nombre")
    short_name = models.CharField(max_length=34, blank=True, verbose_name=u"Nombre corto")
    bg_color = models.CharField(max_length=7, default="#ffffff")
    # Domain
    owner_id = models.IntegerField()
    expires_at = models.DateField(auto_now_add=True)
    status =  models.BooleanField(choices=choices_list.DOMAIN_STATUS, default=True, verbose_name=u"Estado")
    # Contact information
    phone = models.CharField(max_length=32, blank=True, verbose_name=u"Teléfono Principal")
    phones = models.CharField(max_length=64, blank=True, verbose_name=u"Otros Teléfono(s)")
    mobile = models.CharField(max_length=32, blank=True, verbose_name=u"Celular Principal")
    mobiles = models.CharField(max_length=64, blank=True, verbose_name=u"Celulares")
    skype = models.CharField(max_length=32, blank=True)
    email_first = models.EmailField(max_length=64, verbose_name=u"E-mail (1)")
    email_second = models.EmailField(max_length=64, blank=True, verbose_name=u"E-mail (2)")
    culqi_key_public = models.CharField(max_length=64, blank=True, verbose_name=u'Culqi Public Key')
    culqi_key_private = models.CharField(max_length=64, blank=True, verbose_name=u'Culqi Private Key')
    culqi_commission = models.FloatField(default=0, verbose_name=u"Comisión de CULQI (%)")
    visa_link = models.CharField(max_length=200, blank=True)
    visa_commission = models.FloatField(default=0, verbose_name=u"Comisión de Visa (%)")
    paypal_link = models.CharField(max_length=128, blank=True)
    paypal_commission = models.FloatField(default=0, verbose_name=u"Comisión de Paypal (%)")
    # dir
    quantity_emails = models.SmallIntegerField(default=5, verbose_name=u"Cantidad de correos")
    with_www = models.BooleanField(default=False, verbose_name=u"con www")
    company_type = models.CharField(max_length=1, default='p', choices=choices_list.COMPANY_TYPE)
    crm = models.BooleanField(default=False, verbose_name="Cache (Optimizado)")
    payment_gateway = models.BooleanField(default=False)
    template_type = models.CharField(
        max_length=1,
        default="g",
        choices=choices_list.TEMPLATE_CATEGORY,
        verbose_name=u"Tipo de plantillas"
        )
    template = models.ForeignKey(
        Template,
        default=1,
        on_delete=models.SET_DEFAULT
        )
    color = models.ForeignKey(
        Color,
        default=1,
        on_delete=models.SET_DEFAULT)
    logo = models.ImageField(
        upload_to=web_media_dir,
        blank=True,
        default="logo.png"
        )
    favicon = models.ImageField(
        upload_to=web_media_dir,
        blank=True,
        default="favicon.ico"
        )
    mail_server = models.ForeignKey(Server, related_name='mail_server', null=True, on_delete=models.SET_NULL)
    render_server = models.ForeignKey(Server, related_name='render_server', null=True, on_delete=models.SET_NULL)
    opendkim = models.CharField(max_length=255, blank=True)
    webmail = models.BooleanField(default=True)

    class Meta:
        ordering = ["-id"]
        index_together = [["id", "owner_id"],]

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.name:
            self.name = self.domain
        if not self.short_name:
            self.short_name = self.domain

        if not self.code:
            self.code = uuid1().hex
        super(Web,self).save(*args,**kwargs)

    def full_domain(self):
        if self.with_www:
            return "www.{}".format(self.domain)
        return self.domain

    def update_hash(self):
        self.code = uuid1().hex
        self.save()


class Gallery(models.Model):
    image = StdImageField(
        upload_to="gallery",
        blank=True,
        variations={
            'medium': (640, 400, True),
            'thumb': (360, 240, True),
        },
        default = "node.png",
        verbose_name = u"Imagen"
        )
    title = models.CharField(max_length=64)
    def __str__(self):
        return self.title


class Ticket(models.Model):
    domain = models.CharField(max_length=64, verbose_name=u"Dominio")
    phone = models.CharField(max_length=64, blank=True, verbose_name=u"Teléfono")
    is_new = models.BooleanField(default=True, editable=False)
    subject = models.CharField(max_length=64, verbose_name=u"Asunto")
    message = models.TextField(verbose_name=u"Mensaje")
    closed_by = models.IntegerField(default=0, editable=False)
    owner_id = models.IntegerField(editable=False)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.domain

    class Meta:
        ordering = ["-id"]
