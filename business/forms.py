from django.forms import ModelForm, Textarea, CharField, Form
from business.models import Web, Template, Color, Ticket, Gallery, Server


class NewForm(ModelForm):
    class Meta:
        model = Web
        fields = ('domain','name','multi_language', 'language','phone','mobile','email_first','company_type','template_type')

class InfoForm(ModelForm):
    class Meta:
        model = Web
        fields = ('name', 'short_name', 'language', 'protocol', 'multi_language', 'with_www','company_type','status','template_type','crm')

class PaymentForm(ModelForm):
    class Meta:
        model = Web
        fields = ('culqi_key_public','culqi_key_private','culqi_commission','visa_link','visa_commission','paypal_link','paypal_commission','payment_gateway')

class ServerForm(ModelForm):
    class Meta:
        model = Web
        fields = ('render_server','mail_server','opendkim','quantity_emails','webmail')

class NameForm(ModelForm):
    class Meta:
        model = Web
        fields = ('domain',)

class DesignForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(DesignForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        self.fields['template'].queryset = Template.objects.filter(private=False, category=instance.template_type)

    class Meta:
        model = Web
        fields = ('logo','favicon','template','color')

class ChangeOwnerForm(ModelForm):
    class Meta:
        model = Web
        fields = ('owner_id',)

class ContactForm(ModelForm):
    class Meta:
        model = Web
        fields = ('phone','phones','mobile','mobiles','skype','email_first','email_second')

class TemplateForm(ModelForm):
    class Meta:
        model = Template
        fields = ('name',)


class TemplateEditForm(ModelForm):
    class Meta:
        model = Template
        exclude = ('domain',)

class ImportForm(Form):
    data = CharField(widget=Textarea(attrs={'cols': 80, 'rows': 20}))


class ColorForm(ModelForm):
    class Meta:
        model = Color
        exclude = ('domain',)


class TicketForm(ModelForm):
    class Meta:
        model = Ticket
        exclude = ('exclude',)


class GalleryForm(ModelForm):
    class Meta:
        model = Gallery
        exclude = ('exclude',)


class ServerForm(ModelForm):
    class Meta:
        model = Server
        exclude = ('exclude',)