from django.conf.urls import url
from business.views import users, templates, webs, api, tickets, galleries, servers
urlpatterns = [
    url(r'^new$', webs.new, name='webs_new'),
    url(r'^webs/(?P<web_pk>\d+)/change_domain/$', webs.domain_change, name='domains_change'),
    url(r'^webs/(?P<web_pk>\d+)/info/$', webs.info, name='domains_info'),
    url(r'^webs/(?P<web_pk>\d+)/payment/$', webs.payment, name='domains_payment'),
    url(r'^webs/(?P<web_pk>\d+)/server/$', webs.server, name='domains_server'),
    url(r'^webs/(?P<web_pk>\d+)/change_owner/$', webs.change_owner, name='change_owner'),
    url(r'^webs/(?P<web_pk>\d+)/contact/$', webs.contact, name='domains_contact'),
    url(r'^webs/(?P<web_pk>\d+)/design/$', webs.design, name='domains_design'),
    # users
    url(r'^users/$', users.index, name='users'),
    url(r'^users/(?P<pk>\d+)/$', users.detail, name='users_detail'),
    url(r'^users/(?P<pk>\d+)/edit$', users.edit, name='users_edit'),
    url(r'^users/(?P<pk>\d+)/password$', users.password, name='users_password'),
    url(r'^users/new/$', users.new, name='users_new'),
    # templates
    url(r'^templates/$', templates.index, name='templates'),
    url(r'^templates/(?P<pk>\d+)/$', templates.detail, name='templates_detail'),
    url(r'^templates/(?P<pk>\d+)/edit$', templates.edit, name='templates_edit'),
    url(r'^templates/(?P<pk>\d+)/color$', templates.color, name='templates_color'),
    url(r'^templates/(?P<pk>\d+)/color_edit$', templates.color_edit, name='templates_color_edit'),
    url(r'^templates/new$', templates.new, name='templates_new'),
    url(r'^templates/import$', templates.import_config, name='templates_import'),
    url(r'^templates/(?P<template_pk>\d+)/import_colors$', templates.import_colors, name='templates_import_colors'),
    # tickets
    url(r'^tickets/$', tickets.index, name='tickets'),
    url(r'^tickets/new$', tickets.new, name='tickets_new'),
    url(r'^tickets/(?P<pk>\d+)/$', tickets.detail, name='tickets_detail'),
    # galleries
    url(r'^galleries/$', galleries.index, name='galleries'),
    url(r'^galleries/new$', galleries.new, name='galleries_new'),
    url(r'^galleries/(?P<pk>\d+)/update$', galleries.update, name='galleries_update'),
    # API
    url(r'^api/(?P<template_pk>\d+)/colors$', api.colors, name='api_colors'),
    url(r'^api/gallery/$', api.set_gallery, name='set_gallery'),
    # serversa
    url(r'^servers/$', servers.index, name='servers'),
    url(r'^servers/(?P<pk>\d+)/update$', servers.update, name='servers_update'),
    url(r'^servers/new$', servers.new, name='servers_new'),
]