# -*- coding: utf-8 -*-
from django.forms import ModelForm
from common.models import Redirect
class RedirectForm(ModelForm):
    class Meta:
        model = Redirect
        exclude = ('exclude',)