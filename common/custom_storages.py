from simple import config
from storages.backends.s3boto import S3BotoStorage

#class StaticStorage(S3BotoStorage):
#    location = config.STATICFILES_LOCATION

class MediaStorage(S3BotoStorage):
        location = config.MEDIAFILES_LOCATION