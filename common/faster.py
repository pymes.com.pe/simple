from urllib.parse import urlparse

def get_network(value):
    domain = urlparse(value).hostname
    domain = value.strip('www.')
    domain = domain.strip('.com')
    if domain == "plus.google":
        return "google-plus"
    return domain