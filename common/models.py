# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

PROTOCOL = (
    (True,"https://"),
    (False,"http://"),
    )

MONEYS = (
    ('USD',"USD"),
    ('PEN',"S/."),
    )

LANGUAGES = (
    ('es','Español'),
    ('en','English'),
    ('pt-br','Portugues-Brazil'),
    ('de','Aleman'),
    )

DOMAIN_STATUS = (
    (True, "Publicado"),
    (False, "No Publicado"),
    )

NODE_TYPE = (
    ("e","Entrada de blog"),
    ("p","Página básica"),
    ("s","Servicio / producto"),
    ("c","Categoria"),
    ("t","Tour"),
    ("r","Habitación"),
    ("fc","Formulario de contacto"),
    ("fr","Reservar Habitación"),
    ("ft","Reservar Tour"),
    ("g","Galeria de fotos"),
    ("b","Listar entradas de blog"), # Listar entradas de blog
    ("l","Landing Page"), # Landing Page
    )

STATUS = (
    (True, "Publicado"),
    (False, "No Publicado"),
    )

SEX = (
    ('m','Masculino'),
    ('f','Femenino')
    )

DOCUMENT_TYPE = (
    ('d','DNI'),
    ('p','Pasaporte'),
    ('r','RUC'),
    ('o','Otro')
    )

ACCOUNT_TYPE = (
    (True,'Persona'),
    (False,'Compania'),
    )

BLOCK_TYPE = (
    ("t", 'Texto'),
    ("l", 'Imagen a la izquierda'),
    ("r", 'Imagen a la derecha'),
    ("b", 'Imagen de fondo'),
    )

WITH_IMAGE = (
    (True, 'Si'),
    (False, 'No'),
    )

TEMPLATE_CATEGORY = (
    ('g','General'),
    ('t','Agencia de viajes'),
    ('h','Hotel & Hostal'),
    ('s','Tienda'),
    ('o','One Page'),
    ('i','Inmobiliaria'),
    )

COMPANY_TYPE = (
    ('p','Pymes'),
    ('t','Agencia de viajes'),
    ('h','Hotel & Hostal'),
    ('s','Tienda'),
    ('o','One Page'),
    ('i','Inmobiliaria'),
    )

PAYMENT_METHOD = (
    ('b', 'Transferencia bancaria'),
    ('p', 'Paypal'),
    ('y', 'Yape'),
    )

INVOICE_PAID = (
    (False,'Pendiente de pago'),
    (True,'Pagado'),
    )

INVOICE_STATUS = (
    (1,'Renovar Mensual'),
    (2,'Renovar Anual'),
    (3,'Finalizado'),
    )


class Redirect(models.Model):
    web_id = models.IntegerField(editable=False)
    old_path = models.CharField(
        verbose_name=u'redirect from',
        max_length = 200,
        db_index = True,
        help_text = u"Deveria ser una url absoluta, Excluyendo el nombre de dominio. Ejemplo: '/eventos-old'.",
    )
    new_path = models.CharField(
        verbose_name = 'redirect to',
        max_length = 200,
        help_text = u"Deveria ser una url absoluta, Excluyendo el nombre de dominio. Ejemplo: '/eventos-new'.",
    )
    create_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (('web_id', 'old_path'),)
        ordering = ('old_path',)

    def __str__(self):
        return "{0} ---> {1}".format(self.old_path, self.new_path)

class NotFound(models.Model):
    web_id = models.IntegerField(editable=False)
    path = models.CharField(max_length=200, db_index=True)
    count = models.IntegerField(default=1, editable=False)
    update_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = (('web_id', 'path'),)
        ordering = ('-id',)

    def __str__(self):
        return "{0}".format(self.path)
