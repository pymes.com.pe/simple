# -*- coding: utf-8 -*-
from django.contrib import messages
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives


def new_message(message,web):
    ctx = {
        "contact":message,
        "web":web,
    }
    from_email = "%s <%s>" % (web["name"], "notificacion@pymes.com.pe")
    email = web["email_first"] # Web emails
    titulo = "%s (%s)" % (message.subject, message.pk)
    ctn_html = render_to_string('mail/new_message.html', ctx)
    ctn_text = render_to_string('mail/new_message.txt', ctx)
    msg = EmailMultiAlternatives(titulo, ctn_text, from_email, to=[email], reply_to=[message.email])
    msg.attach_alternative(ctn_html, 'text/html')
    #msg.content_subtype = "html"
    msg.send()
    return True

def new_book_room(message,web):
    ctx = {
        "message":message,
        "web":web,
    }
    from_email = "%s <%s>" % (web["name"], "notificacion@pymes.com.pe")
    email = web["email_first"] # Web emails
    titulo = "%s %s" % (message.room, message.pk)
    ctn_html = render_to_string('mail/book_room.html', ctx)
    ctn_text = render_to_string('mail/book_room.txt', ctx)
    msg = EmailMultiAlternatives(titulo, ctn_text, from_email, to=[email], reply_to=[message.email])
    msg.attach_alternative(ctn_html, 'text/html')
    #msg.content_subtype = "html"
    msg.send()
    return True

def new_book_tour(message,web):
    ctx = {
        "message":message,
        "web":web,
    }
    from_email = "%s <%s>" % (web["name"], "notificacion@pymes.com.pe")
    email = web["email_first"] # Web emails
    titulo = "%s %s" % (message.tour, message.pk)
    ctn_html = render_to_string('mail/book_tour.html', ctx)
    ctn_text = render_to_string('mail/book_tour.txt', ctx)
    msg = EmailMultiAlternatives(titulo, ctn_text, from_email, to=[email], reply_to=[message.email])
    msg.attach_alternative(ctn_html, 'text/html')
    #msg.content_subtype = "html"
    msg.send()
    return True
